<?php

/**
 * @file
 * Post update functions for Plugin reference.
 */

use Drupal\Core\Config\Entity\ConfigEntityUpdater;
use Drupal\Core\Entity\Display\EntityFormDisplayInterface;
use Drupal\field\FieldConfigInterface;

/**
 * Add configuration_form to the plugin reference widgets.
 */
function pluginreference_post_update_add_configuration_form_option_to_widget(array &$sandbox = NULL) {
  /** @var \Drupal\Core\Config\Entity\ConfigEntityUpdater $config_entity_updater */
  $config_entity_updater = \Drupal::classResolver(ConfigEntityUpdater::class);
  $config_entity_updater->update($sandbox, 'entity_form_display', function (EntityFormDisplayInterface $form_display) {
    $pluginreference_widgets = [
      'plugin_reference_autocomplete',
      'plugin_reference_select',
      'plugin_reference_options_buttons',
    ];

    $updated = FALSE;
    foreach ($form_display->getComponents() as $id => $component) {
      if (isset($component['type']) && in_array($component['type'], $pluginreference_widgets, TRUE) && !isset($component['settings']['configuration_form'])) {
        $component['settings']['configuration_form'] = 'hidden';
        $form_display->setComponent($id, $component);
        $updated = TRUE;
      }
    }

    return $updated;
  });
}

/**
 * Add match_operator and match_limit to plugin_reference_autocomplete widgets.
 */
function pluginreference_post_update_add_match_options_to_autocomplete_widget(array &$sandbox = NULL) {
  /** @var \Drupal\Core\Config\Entity\ConfigEntityUpdater $config_entity_updater */
  $config_entity_updater = \Drupal::classResolver(ConfigEntityUpdater::class);
  $config_entity_updater->update($sandbox, 'entity_form_display', function (EntityFormDisplayInterface $form_display) {

    $updated = FALSE;
    foreach ($form_display->getComponents() as $id => $component) {
      if (isset($component['type']) && $component['type'] === 'plugin_reference_autocomplete' && !isset($component['settings']['match_operator'])) {
        $component['settings']['match_operator'] = 'CONTAINS';
        $component['settings']['match_limit'] = 10;
        $form_display->setComponent($id, $component);
        $updated = TRUE;
      }
    }

    return $updated;
  });
}

/**
 * Add handler and handler_settings to the plugin_reference field configs.
 */
function pluginreference_post_update_add_handler_to_plugin_reference_field_configs(array &$sandbox = NULL) {
  /** @var \Drupal\Core\Config\Entity\ConfigEntityUpdater $config_entity_updater */
  $config_entity_updater = \Drupal::classResolver(ConfigEntityUpdater::class);
  $config_entity_updater->update($sandbox, 'field_config', function (FieldConfigInterface $field_config) {
    $updated = FALSE;

    if ($field_config->getType() === 'plugin_reference' && $field_config->getSetting('handler') === NULL) {
      $settings = $field_config->getSettings();
      $target_type = $field_config->getFieldStorageDefinition()->getSetting('target_type');
      $settings['handler'] = 'default:' . $target_type;
      $settings['handler_settings']['sort'] = [
        'key' => 'label',
        'direction' => 'ASC',
      ];
      $field_config->set('settings', $settings);
      $updated = TRUE;
    }

    return $updated;
  });
}
