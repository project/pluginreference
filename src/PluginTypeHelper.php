<?php

namespace Drupal\pluginreference;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Extension\ExtensionList;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Helper class to simplify plugin related code.
 */
class PluginTypeHelper implements PluginTypeHelperInterface {

  /**
   * The service container.
   *
   * The service container is passed because there is no other way to identify
   * plugin managers than looping over all service IDs.
   *
   * @var \Symfony\Component\DependencyInjection\ContainerInterface
   */
  protected $container;

  /**
   * The module extension list..
   *
   * @var \Drupal\Core\Extension\ExtensionList
   */
  protected $moduleExtensionList;

  /**
   * Prefix that is used to identify a service as a plugin manager.
   */
  protected const PLUGIN_MANAGER_SERVICE_ID_PREFIX = 'plugin.manager.';

  /**
   * Property that stores all found plugin type Ids.
   *
   * @var array
   */
  protected $pluginTypeIds = [];

  /**
   * PluginTypeHelper constructor.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The service container.
   * @param \Drupal\Core\Extension\ExtensionList $moduleExtensionList
   *   The module extension list.
   */
  public function __construct(ContainerInterface $container, ExtensionList $moduleExtensionList) {
    $this->container = $container;
    $this->moduleExtensionList = $moduleExtensionList;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginTypeIds(): array {
    if (empty($this->pluginTypeIds)) {
      foreach ($this->container->getServiceIds() as $service_id) {
        if (strpos($service_id, self::PLUGIN_MANAGER_SERVICE_ID_PREFIX) === 0) {
          $this->pluginTypeIds[] = str_replace('plugin.manager.', '', $service_id);
        }
      }
    }

    return $this->pluginTypeIds;
  }

  /**
   * {@inheritdoc}
   */
  public function pluginTypeExists(string $plugin_type_id): bool {
    $plugin_type_ids = $this->getPluginTypeIds();
    return in_array($plugin_type_id, $plugin_type_ids);
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginTypeName(string $plugin_type_id): string {
    return ucwords(str_replace(['.', '_'], ' ', $plugin_type_id));
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginTypeProvider(string $plugin_type_id): ?string {
    $plugin_manager = $this->getPluginManager($plugin_type_id);

    if (!$plugin_manager instanceof PluginManagerInterface) {
      return NULL;
    }

    $class = get_class($plugin_manager);
    $class_parts = preg_split('/\\\/', $class);

    if (!is_array($class_parts) || !array_key_exists(1, $class_parts)) {
      return NULL;
    }

    $provider = $class_parts[1];

    if (strtolower($provider) === 'core') {
      $provider = 'system';
    }

    return $provider;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginTypeProviderName(string $plugin_type_id): ?string {
    $provider = $this->getPluginTypeProvider($plugin_type_id);
    return $provider !== NULL ? $this->getProviderName($provider) : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getProviderName(string $provider): string {
    if (strtolower($provider) === 'core') {
      $provider = 'system';
    }

    return $this->moduleExtensionList->getName($provider);
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginTypeOptions(): array {
    $options = [];

    foreach ($this->getPluginTypeIds() as $plugin_type_id) {
      $options[$this->getPluginTypeProviderName($plugin_type_id)][$plugin_type_id] = $this->getPluginTypeName($plugin_type_id);
    }

    // Sort the referenceable plugins alphabetically.
    ksort($options);

    foreach ($options as $key => $optgroup) {
      ksort($options[$key]);
    }

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginDefinitions(string $plugin_type_id): array {
    $plugin_manager = $this->getPluginManager($plugin_type_id);

    if (!$plugin_manager instanceof PluginManagerInterface) {
      return [];
    }

    return $plugin_manager->getDefinitions();
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginLabel(array $plugin_definition): string {
    return $plugin_definition['label'] ?? $plugin_definition['admin_label'] ?? $plugin_definition['id'];
  }

  /**
   * {@inheritdoc}
   */
  public function hasPluginAccessControl(array $plugin_definition): bool {
    return isset($plugin_definition['class']) && method_exists($plugin_definition['class'], 'access');
  }

  /**
   * {@inheritdoc}
   */
  public function isPluginCacheable(array $plugin_definition): bool {
    return isset($plugin_definition['class']) && method_exists($plugin_definition['class'], 'getCacheContexts');
  }

  /**
   * {@inheritdoc}
   */
  public function isPluginConfigurable(array $plugin_definition): bool {
    return isset($plugin_definition['class']) && method_exists($plugin_definition['class'], 'buildConfigurationForm');
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginManager(string $plugin_type_id): ?PluginManagerInterface {
    if (!$this->container->has(self::PLUGIN_MANAGER_SERVICE_ID_PREFIX . $plugin_type_id)) {
      return NULL;
    }

    $plugin_manager = $this->container->get(self::PLUGIN_MANAGER_SERVICE_ID_PREFIX . $plugin_type_id);
    if (!$plugin_manager instanceof PluginManagerInterface) {
      return NULL;
    }

    return $plugin_manager;
  }

}
