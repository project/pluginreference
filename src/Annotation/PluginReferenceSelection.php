<?php

namespace Drupal\pluginreference\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a plugin reference selection item annotation object.
 *
 * Plugin Namespace: Plugin\PluginReferenceSelection.
 *
 * @see \Drupal\pluginreference\PluginReferenceSelectionManagerInterface
 * @see plugin_api
 *
 * @Annotation
 */
class PluginReferenceSelection extends Plugin {

  /**
   * The plugin ID.
   *
   * There are some implementation bugs that make the plugin available only if
   * the ID follows a specific pattern. It must be either identical to group or
   * prefixed with the group. E.g. if the group is "foo" the ID must be either
   * "foo" or "foo:bar".
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the selection plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * An array of plugin types that can be referenced by this plugin.
   *
   * Defaults to all plugin types.
   *
   * @var array
   */
  public $plugin_types = [];

  /**
   * The selection plugin group.
   *
   * @var string
   *
   * This property is used to allow selection plugins to target a specific
   * plugin type while also inheriting the code of an existing selection plugin.
   * For example, if we want to override the block selection from the 'default'
   * selection type, we can define the annotation of a new plugin as follows:
   * @code
   * id = "default:block_advanced",
   * plugin_types = {"block"},
   * group = "default",
   * weight = 5
   * @endcode
   */
  public $group;

  /**
   * The weight of the plugin in its group.
   *
   * This property is used to select the "best" plugin within a group.
   *
   * @var int
   */
  public $weight;

}
