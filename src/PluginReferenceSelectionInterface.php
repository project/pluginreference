<?php

namespace Drupal\pluginreference;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Component\Plugin\DependentPluginInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Plugin\PluginFormInterface;

/**
 * Interface definition for Plugin Reference Selection plugins.
 *
 * @see \Drupal\pluginreference\PluginReferenceSelectionManagerInterface
 * @see \Drupal\pluginreference\Annotation\PluginReferenceSelection
 * @see plugin_api
 */
interface PluginReferenceSelectionInterface extends PluginInspectionInterface, PluginFormInterface, ConfigurableInterface, DependentPluginInterface {

  /**
   * Gets the list of referenceable plugins.
   *
   * @param string|null $match
   *   (optional) Text to match the label against. Defaults to NULL.
   * @param string $match_operator
   *   (optional) Operator to be used for string matching. Defaults to
   *   "CONTAINS".
   * @param int $limit
   *   (optional) Limit the query to a given number of items. Defaults to 0,
   *   which indicates no limiting.
   *
   * @return array
   *   A nested array of plugins, the first level is keyed by the
   *   provider, which contains an array of plugin labels (escaped),
   *   keyed by the plugin ID.
   */
  public function getReferenceablePlugins(?string $match = NULL, string $match_operator = 'CONTAINS', int $limit = 0): array;

  /**
   * Counts plugins that are referenceable.
   *
   * @param string|null $match
   *   (optional) Text to match the label against. Defaults to NULL.
   * @param string $match_operator
   *   (optional) Operator to be used for string matching. Defaults to
   *   "CONTAINS".
   *
   * @return int
   *   The number of referenceable plugins.
   */
  public function countReferenceablePlugins(?string $match = NULL, string $match_operator = 'CONTAINS'): int;

  /**
   * Validates which existing plugins can be referenced.
   *
   * @param array $ids
   *   An array of IDs to validate.
   *
   * @return array
   *   An array of valid plugin IDs.
   */
  public function validateReferenceablePlugins(array $ids): array;

  /**
   * Informs the plugin that a dependency of the given field will be deleted.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition.
   * @param array $dependencies
   *   An array of dependencies that will be deleted keyed by dependency type.
   *   Dependency types are, for example, entity, module and theme.
   *
   * @return bool
   *   TRUE if the field definition has been changed as a result, FALSE if not.
   *
   * @see \Drupal\pluginreference\Plugin\Field\FieldType\PluginReferenceItem::onDependencyRemoval()
   */
  public function onDependencyRemoval(FieldDefinitionInterface $field_definition, array $dependencies): bool;

}
