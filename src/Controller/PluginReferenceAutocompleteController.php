<?php

namespace Drupal\pluginreference\Controller;

use Drupal\Component\Utility\Crypt;
use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\Tags;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\KeyValueStore\KeyValueStoreInterface;
use Drupal\Core\Site\Settings;
use Drupal\pluginreference\PluginReferenceSelectionManagerInterface;
use Drupal\pluginreference\PluginTypeHelperInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Controller class for the plugin reference autocomplete form element.
 *
 * @package Drupal\pluginreference\Controller
 */
class PluginReferenceAutocompleteController extends ControllerBase {

  /**
   * The plugin type helper.
   *
   * @var \Drupal\pluginreference\PluginTypeHelperInterface
   */
  protected $pluginTypeHelper;

  /**
   * The plugin reference selection manager.
   *
   * @var \Drupal\pluginreference\PluginReferenceSelectionManagerInterface
   */
  protected $pluginReferenceSelectionManager;

  /**
   * The key value store.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreInterface
   */
  protected $keyValue;

  /**
   * PluginReferenceAutocompleteController constructor.
   *
   * @param \Drupal\pluginreference\PluginTypeHelperInterface $plugin_type_helper
   *   The plugin type helper.
   * @param \Drupal\pluginreference\PluginReferenceSelectionManagerInterface $plugin_reference_selection_manager
   *   The plugin reference selection manager.
   * @param \Drupal\Core\KeyValueStore\KeyValueStoreInterface $key_value
   *   The key value factory.
   */
  public function __construct(PluginTypeHelperInterface $plugin_type_helper, PluginReferenceSelectionManagerInterface $plugin_reference_selection_manager, KeyValueStoreInterface $key_value) {
    $this->pluginTypeHelper = $plugin_type_helper;
    $this->pluginReferenceSelectionManager = $plugin_reference_selection_manager;
    $this->keyValue = $key_value;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin_reference.plugin_type_helper'),
      $container->get('plugin.manager.plugin_reference_selection'),
      $container->get('keyvalue')->get('plugin_autocomplete')
    );
  }

  /**
   * Autocomplete callback.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   * @param string $target_type
   *   The plugin type to filter results.
   * @param string $selection_handler
   *   The ID ID of the plugin reference selection handler.
   * @param string $selection_settings_key
   *   The hashed key of the key/value entry that holds the selection handler
   *   settings.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   JSON autocomplete response.
   */
  public function handleAutocomplete(Request $request, string $target_type, string $selection_handler, string $selection_settings_key) {
    $matches = [];

    if ($this->pluginTypeHelper->pluginTypeExists($target_type) && $input = $request->query->get('q')) {
      // Selection settings are passed in as a hashed key of a serialized array
      // stored in the key/value store.
      $selection_settings = $this->keyValue->get($selection_settings_key, FALSE);
      if ($selection_settings !== FALSE) {
        $selection_settings_hash = Crypt::hmacBase64(serialize($selection_settings) . $target_type . $selection_handler, Settings::getHashSalt());
        if (!hash_equals($selection_settings_hash, $selection_settings_key)) {
          // Disallow access when the selection settings hash does not match the
          // passed-in key.
          throw new AccessDeniedHttpException('Invalid selection settings key.');
        }
      }
      else {
        // Disallow access when the selection settings key is not found in the
        // key/value store.
        throw new AccessDeniedHttpException();
      }

      $options = $selection_settings + [
        'target_type' => $target_type,
        'handler' => $selection_handler,
      ];
      /** @var \Drupal\pluginreference\PluginReferenceSelectionInterface $handler */
      $handler = $this->pluginReferenceSelectionManager->getInstance($options);
      $match_operator = !empty($selection_settings['match_operator']) ? $selection_settings['match_operator'] : 'CONTAINS';
      $match_limit = isset($selection_settings['match_limit']) ? (int) $selection_settings['match_limit'] : 10;
      $plugin_labels = $handler->getReferenceablePlugins($input, $match_operator, $match_limit);

      // Loop through the plugins and convert them into autocomplete output.
      foreach ($plugin_labels as $plugin_id => $label) {
        $key = "$label ($plugin_id)";
        // Strip things like starting/trailing white spaces, line breaks and
        // tags.
        $key = preg_replace('/\s\s+/', ' ', str_replace("\n", '', trim(Html::decodeEntities(strip_tags($key)))));
        // Names containing commas or quotes must be wrapped in quotes.
        $key = Tags::encode((string) $key);
        $matches[] = ['value' => $key, 'label' => $label];
      }
    }

    return new JsonResponse($matches);
  }

}
