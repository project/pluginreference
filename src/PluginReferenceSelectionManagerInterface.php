<?php

namespace Drupal\pluginreference;

use Drupal\Component\Plugin\FallbackPluginManagerInterface;
use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;

/**
 * Defines an interface for the plugin reference selection plugin manager.
 */
interface PluginReferenceSelectionManagerInterface extends PluginManagerInterface, FallbackPluginManagerInterface {

  /**
   * Gets the plugin ID for a given target plugin type and base plugin ID.
   *
   * @param string $target_type
   *   The target plugin type.
   * @param string $base_plugin_id
   *   The base plugin ID (e.g. 'default').
   *
   * @return string
   *   The plugin ID.
   */
  public function getPluginId($target_type, $base_plugin_id): string;

  /**
   * Gets the selection plugins that can reference a specific plugin type.
   *
   * @param string $plugin_type_id
   *   The plugin type ID.
   *
   * @return array
   *   An array of selection plugins grouped by selection group.
   */
  public function getSelectionGroups(string $plugin_type_id): array;

  /**
   * Gets the selection handler for a given plugin_reference field.
   *
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The field definition for the operation.
   * @param \Drupal\Core\Entity\EntityInterface|null $entity
   *   (optional) The entity for the operation. Defaults to NULL.
   *
   * @return \Drupal\pluginreference\PluginReferenceSelectionInterface|null
   *   The selection plugin when available, else NULL.
   */
  public function getSelectionHandler(FieldDefinitionInterface $field_definition, EntityInterface $entity = NULL): ?PluginReferenceSelectionInterface;

}
