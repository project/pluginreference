<?php

namespace Drupal\pluginreference;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Plugin type manager for plugin Reference Selection plugins.
 *
 * @see \Drupal\pluginreference\Annotation\PluginReferenceSelection
 * @see \Drupal\pluginreference\PluginReferenceSelectionInterface
 * @see plugin_api
 */
class PluginReferenceSelectionManager extends DefaultPluginManager implements PluginReferenceSelectionManagerInterface {

  /**
   * {@inheritdoc}
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    $this->alterInfo('plugin_reference_selection');
    $this->setCacheBackend($cache_backend, 'plugin_reference_selection_plugins');

    parent::__construct('Plugin/PluginReferenceSelection', $namespaces, $module_handler, 'Drupal\pluginreference\PluginReferenceSelectionInterface', 'Drupal\pluginreference\Annotation\PluginReferenceSelection');
  }

  /**
   * {@inheritdoc}
   */
  public function getInstance(array $options) {
    if (!isset($options['target_type'])) {
      throw new \InvalidArgumentException("Missing required 'target_type' property for a PluginReferenceSelection plugin.");
    }

    // Initialize default options.
    $options += [
      'handler' => $this->getPluginId($options['target_type'], 'default'),
    ];

    // A specific selection plugin ID was already specified.
    if (strpos($options['handler'], ':') !== FALSE) {
      $plugin_id = $options['handler'];
    }
    // Only a selection group name was specified.
    else {
      $plugin_id = $this->getPluginId($options['target_type'], $options['handler']);
    }
    unset($options['handler']);

    return $this->createInstance($plugin_id, $options);
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginId($target_type, $base_plugin_id): string {
    // Get all available selection plugins for this plugin_type.
    $selection_handler_groups = $this->getSelectionGroups($target_type);

    // Sort the selection plugins by weight and select the best match.
    uasort($selection_handler_groups[$base_plugin_id], 'Drupal\Component\Utility\SortArray::sortByWeightElement');
    end($selection_handler_groups[$base_plugin_id]);
    return key($selection_handler_groups[$base_plugin_id]);
  }

  /**
   * {@inheritdoc}
   */
  public function getSelectionGroups(string $plugin_type_id): array {
    $plugins = [];
    $definitions = $this->getDefinitions();

    // Do not display the 'broken' plugin in the UI.
    unset($definitions['broken']);

    foreach ($definitions as $plugin_id => $plugin) {
      if (empty($plugin['plugin_types']) || in_array($plugin_type_id, $plugin['plugin_types'])) {
        $plugins[$plugin['group']][$plugin_id] = $plugin;
      }
    }

    ksort($plugins);

    return $plugins;
  }

  /**
   * {@inheritdoc}
   */
  public function getSelectionHandler(FieldDefinitionInterface $field_definition, EntityInterface $entity = NULL): ?PluginReferenceSelectionInterface {
    $options = $field_definition->getSetting('handler_settings') ?: [];
    $options += [
      'target_type' => $field_definition->getFieldStorageDefinition()->getSetting('target_type'),
      'handler' => $field_definition->getSetting('handler'),
      'entity' => $entity,
    ];

    $selection_handler = $this->getInstance($options);
    if (!$selection_handler instanceof PluginReferenceSelectionInterface) {
      return NULL;
    }

    return $selection_handler;
  }

  /**
   * {@inheritdoc}
   */
  public function getFallbackPluginId($plugin_id, array $configuration = []) {
    return 'broken';
  }

}
