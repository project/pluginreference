<?php

namespace Drupal\pluginreference;

use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * Interface for the plugin reference manager.
 */
interface PluginTypeHelperInterface {

  /**
   * Get all plugin type IDs.
   *
   * @return array
   *   An array of plugin type IDs.
   */
  public function getPluginTypeIds(): array;

  /**
   * Check if the given plugin type exists.
   *
   * @param string $plugin_type_id
   *   The plugin type ID.
   *
   * @return bool
   *   True when the plugin type exists, else false.
   */
  public function pluginTypeExists(string $plugin_type_id): bool;

  /**
   * Get a human-readable name for the given plugin type.
   *
   * @param string $plugin_type_id
   *   The plugin type ID.
   *
   * @return string
   *   The human-readable name.
   */
  public function getPluginTypeName(string $plugin_type_id): string;

  /**
   * The system name of the provider of the plugin type.
   *
   * This is the module defining the plugin type manager.
   *
   * @param string $plugin_type_id
   *   The plugin type ID.
   *
   * @return string|null
   *   The system name of the provider when available.
   */
  public function getPluginTypeProvider(string $plugin_type_id): ?string;

  /**
   * The clean name of the provider of the plugin type.
   *
   * This is the module defining the plugin type manager.
   *
   * @param string $plugin_type_id
   *   The plugin type ID.
   *
   * @return string|null
   *   The clean name of the provider when available.
   */
  public function getPluginTypeProviderName(string $plugin_type_id): ?string;

  /**
   * Converts the provider to a clean name used in the UI.
   *
   * @param string $provider
   *   The provider.
   *
   * @return string
   *   The clean provider name.
   */
  public function getProviderName(string $provider): string;

  /**
   * Get an array of plugin types that can be used in form elements.
   *
   * @return array
   *   An array of plugin type labels, keyed by ID.
   */
  public function getPluginTypeOptions(): array;

  /**
   * Get all plugin definitions of the given plugin type.
   *
   * @param string $plugin_type_id
   *   The plugin type ID.
   *
   * @return array
   *   An array containing plugin definitions.
   */
  public function getPluginDefinitions(string $plugin_type_id): array;

  /**
   * Get the plugin label by the plugin definition.
   *
   * @param array $plugin_definition
   *   The plugin definition.
   *
   * @return string
   *   The label of the plugin.
   */
  public function getPluginLabel(array $plugin_definition): string;

  /**
   * Checks if the given plugin definition has access control implemented.
   *
   * @param array $plugin_definition
   *   The plugin definition.
   *
   * @return bool
   *   True when the plugin has access control, else FALSE.
   */
  public function hasPluginAccessControl(array $plugin_definition): bool;

  /**
   * Checks if the given plugin is cacheable.
   *
   * @param array $plugin_definition
   *   The plugin definition.
   *
   * @return bool
   *   True when the plugin is cacheable, else FALSE.
   */
  public function isPluginCacheable(array $plugin_definition): bool;

  /**
   * Checks if the given plugin definition is configurable.
   *
   * @param array $plugin_definition
   *   The plugin definition.
   *
   * @return bool
   *   True when the plugin is configurable, else FALSE.
   */
  public function isPluginConfigurable(array $plugin_definition): bool;

  /**
   * Get the plugin manager of the given plugin type.
   *
   * @param string $plugin_type_id
   *   The plugin type ID.
   *
   * @return \Drupal\Component\Plugin\PluginManagerInterface|null
   *   The plugin manager, when exists.
   */
  public function getPluginManager(string $plugin_type_id): ?PluginManagerInterface;

}
