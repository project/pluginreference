<?php

namespace Drupal\pluginreference\Plugin\PluginReferenceSelection;

use Drupal\pluginreference\PluginReferenceSelectionBase;

/**
 * Defines a fallback plugin for missing plugin_reference_selection plugins.
 *
 * @PluginReferenceSelection(
 *   id = "broken",
 *   label = @Translation("Broken/Missing")
 * )
 */
class Broken extends PluginReferenceSelectionBase {

  /**
   * {@inheritdoc}
   */
  public function getReferenceablePlugins(?string $match = NULL, string $match_operator = 'CONTAINS', int $limit = 0): array {
    return [];
  }

}
