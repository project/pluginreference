<?php

namespace Drupal\pluginreference\Plugin\PluginReferenceSelection;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\pluginreference\PluginTypeHelperInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the default selection for block plugins.
 *
 * @PluginReferenceSelection(
 *   id = "default:block",
 *   label = @Translation("Default"),
 *   plugin_types = {"block"},
 *   group = "default",
 *   weight = 1
 * )
 */
class BlockSelection extends DefaultSelection {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, PluginTypeHelperInterface $plugin_type_helper, AccountProxyInterface $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $plugin_type_helper);

    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin_reference.plugin_type_helper'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function filterReferenceablePluginDefinitions(array &$plugin_definitions): void {
    $target_type = $this->getConfiguration()['target_type'];
    $plugin_manager = $this->pluginTypeHelper->getPluginManager($target_type);

    if (!$plugin_manager instanceof PluginManagerInterface) {
      return;
    }

    foreach ($plugin_definitions as $plugin_id => $plugin_definition) {
      /** @var \Drupal\Core\Block\BlockPluginInterface $block_plugin */
      $block_plugin = $plugin_manager->createInstance($plugin_id);

      if (!$block_plugin->access($this->currentUser)) {
        unset($plugin_definitions[$plugin_id]);
      }
    }
  }

}
