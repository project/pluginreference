<?php

namespace Drupal\pluginreference\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Plugin Reference valid reference constraint.
 *
 * Verifies that referenced plugins are valid.
 *
 * @Constraint(
 *   id = "ValidPluginReference",
 *   label = @Translation("Plugin Reference valid reference", context = "Validation")
 * )
 */
class ValidPluginReferenceConstraint extends Constraint {

  /**
   * The default violation message.
   *
   * @var string
   */
  public $message = 'This plugin (%type: %id) cannot be referenced.';

  /**
   * Violation message when the plugin does not exist.
   *
   * @var string
   */
  public $nonExistingMessage = 'The referenced plugin (%type: %id) does not exist.';

}
