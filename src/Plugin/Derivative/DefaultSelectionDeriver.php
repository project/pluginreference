<?php

namespace Drupal\pluginreference\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\pluginreference\PluginTypeHelperInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides derivative plugins for the DefaultSelection plugin.
 *
 * @see \Drupal\pluginreference\Plugin\PluginReferenceSelection\DefaultSelection
 * @see \Drupal\pluginreference\PluginReferenceSelectionManagerInterface
 * @see \Drupal\pluginreference\Annotation\PluginReferenceSelection
 * @see \Drupal\pluginreference\PluginReferenceSelectionInterface
 * @see plugin_api
 */
class DefaultSelectionDeriver extends DeriverBase implements ContainerDeriverInterface {

  /**
   * The plugin type helper.
   *
   * @var \Drupal\pluginreference\PluginTypeHelperInterface
   */
  protected $pluginTypeHelper;

  /**
   * Creates a DefaultSelectionDeriver object.
   *
   * @param \Drupal\pluginreference\PluginTypeHelperInterface $plugin_type_helper
   *   The plugin type helper.
   */
  public function __construct(PluginTypeHelperInterface $plugin_type_helper) {
    $this->pluginTypeHelper = $plugin_type_helper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('plugin_reference.plugin_type_helper')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    foreach ($this->pluginTypeHelper->getPluginTypeIds() as $plugin_type_id) {
      $this->derivatives[$plugin_type_id] = $base_plugin_definition;
      $this->derivatives[$plugin_type_id]['plugin_types'] = [$plugin_type_id];
      $this->derivatives[$plugin_type_id]['label'] = t('@plugin_type selection', ['@plugin_type' => $this->pluginTypeHelper->getPluginTypeName($plugin_type_id)]);
      $this->derivatives[$plugin_type_id]['base_plugin_label'] = (string) $base_plugin_definition['label'];
    }

    return parent::getDerivativeDefinitions($base_plugin_definition);
  }

}
