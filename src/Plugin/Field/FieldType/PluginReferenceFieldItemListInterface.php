<?php

namespace Drupal\pluginreference\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemListInterface;

/**
 * Interface for plugin reference lists of field items.
 */
interface PluginReferenceFieldItemListInterface extends FieldItemListInterface {

  /**
   * Gets the plugins referenced by this field, preserving field item deltas.
   *
   * @return \Drupal\Component\Plugin\PluginBase[]
   *   An array of plugins keyed by field item deltas.
   */
  public function referencedPlugins();

}
