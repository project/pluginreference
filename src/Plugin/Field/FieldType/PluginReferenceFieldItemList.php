<?php

namespace Drupal\pluginreference\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemList;

/**
 * Defines an item list class for plugin reference fields.
 */
class PluginReferenceFieldItemList extends FieldItemList implements PluginReferenceFieldItemListInterface {

  /**
   * {@inheritdoc}
   */
  public function getConstraints() {
    $constraints = parent::getConstraints();
    $constraint_manager = $this->getTypedDataManager()->getValidationConstraintManager();
    $constraints[] = $constraint_manager->create('ValidPluginReference', []);
    return $constraints;
  }

  /**
   * {@inheritdoc}
   */
  public function referencedPlugins() {
    if ($this->isEmpty()) {
      return [];
    }

    // Collect the IDs of existing plugins to load, and directly grab the
    // "autocreate" entities that are already populated in $item->entity.
    $target_plugins = [];
    /** @var \Drupal\pluginreference\Plugin\Field\FieldType\PluginReferenceItem $item */
    foreach ($this->list as $delta => $item) {
      $plugin = $item->referencedPlugin();
      if ($plugin) {
        $target_plugins[$delta] = $plugin;
      }
    }

    ksort($target_plugins);

    return $target_plugins;
  }

}
