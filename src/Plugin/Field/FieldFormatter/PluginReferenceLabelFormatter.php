<?php

namespace Drupal\pluginreference\Plugin\Field\FieldFormatter;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\pluginreference\PluginTypeHelperInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'plugin_reference_label' formatter.
 *
 * @FieldFormatter(
 *   id = "plugin_reference_label",
 *   label = @Translation("Label"),
 *   field_types = {
 *     "plugin_reference"
 *   }
 * )
 */
class PluginReferenceLabelFormatter extends FormatterBase {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The plugin type helper.
   *
   * @var \Drupal\pluginreference\PluginTypeHelperInterface
   */
  protected $pluginTypeHelper;

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, AccountProxyInterface $current_user, PluginTypeHelperInterface $plugin_type_helper) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);

    $this->currentUser = $current_user;
    $this->pluginTypeHelper = $plugin_type_helper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('current_user'),
      $container->get('plugin_reference.plugin_type_helper')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    $target_type = $items->getFieldDefinition()->getFieldStorageDefinition()->getSetting('target_type');
    $plugin_manager = $this->pluginTypeHelper->getPluginManager($target_type);
    if (!$plugin_manager instanceof PluginManagerInterface) {
      return [];
    }

    foreach ($items as $delta => $item) {
      if (!$plugin_manager->hasDefinition($item->plugin_id)) {
        continue;
      }

      $plugin_definition = $plugin_manager->getDefinition($item->plugin_id);
      // Check access when the plugin has an access method.
      if ($this->pluginTypeHelper->hasPluginAccessControl($plugin_definition)) {
        $plugin = $item->referencedPlugin();
        $access = $plugin->access($this->currentUser->getAccount(), TRUE);
        CacheableMetadata::createFromRenderArray($elements)
          ->addCacheableDependency($access)
          ->applyTo($elements);

        if (!$access->isAllowed()) {
          continue;
        }
      }

      $label = $plugin_definition['label'] ?? NULL;
      if (method_exists($plugin_definition['class'], 'label')) {
        $plugin = $item->referencedPlugin();
        $label = $plugin->label();
      }

      $elements[$delta] = [
        '#markup' => $label,
      ];

      if ($this->pluginTypeHelper->isPluginCacheable($plugin_definition)) {
        $plugin = $item->referencedPlugin();
        CacheableMetadata::createFromRenderArray($elements[$delta])
          ->addCacheableDependency($plugin)
          ->applyTo($elements[$delta]);
      }
    }

    return $elements;
  }

}
