<?php

namespace Drupal\pluginreference;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginBase;

/**
 * Provides a base class for configurable plugin selection handlers.
 */
abstract class PluginReferenceSelectionBase extends PluginBase implements PluginReferenceSelectionInterface {

  /**
   * Constructs a new selection object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->setConfiguration($configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'target_type' => NULL,
      'entity' => NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    // Merge in defaults.
    $this->configuration = NestedArray::mergeDeep(
      $this->defaultConfiguration(),
      $configuration
    );
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function onDependencyRemoval(FieldDefinitionInterface $field_definition, array $dependencies): bool {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {}

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {}

  /**
   * {@inheritdoc}
   */
  public function countReferenceablePlugins(?string $match = NULL, string $match_operator = 'CONTAINS'): int {
    $plugins = $this->getReferenceablePlugins($match, $match_operator);
    return count($plugins);
  }

  /**
   * {@inheritdoc}
   */
  public function validateReferenceablePlugins(array $ids): array {
    $plugins = $this->getReferenceablePlugins();

    $valid_ids = [];
    foreach ($ids as $id) {
      if (isset($plugins[$id])) {
        $valid_ids[] = $id;
      }
    }

    return $valid_ids;
  }

}
