<?php

// @codingStandardsIgnoreFile

/**
 * @file
 * Contains database additions to drupal-8.8.0.bare.standard.php.gz.
 */

use Drupal\Core\Database\Database;
use Drupal\Core\Serialization\Yaml;
use Drupal\field\Entity\FieldStorageConfig;

$connection = Database::getConnection();

// Add 9000 as the latest installed hook_update_N.
$connection->insert('key_value')
  ->fields([
    'collection',
    'name',
    'value',
  ])
  ->values([
    'collection' => 'system.schema',
    'name' => 'pluginreference',
    'value' => 'i:9000;',
  ])
  ->execute();

// Update core.extension. Add pluginreference module.
$extensions = $connection->select('config')
  ->fields('config', ['data'])
  ->condition('collection', '')
  ->condition('name', 'core.extension')
  ->execute()
  ->fetchField();
$extensions = unserialize($extensions);
$extensions['module']['pluginreference'] = 0;
$connection->update('config')
  ->fields([
    'data' => serialize($extensions),
  ])
  ->condition('collection', '')
  ->condition('name', 'core.extension')
  ->execute();

$field_block_storage = Yaml::decode(file_get_contents(__DIR__ . '/field.storage.node.field_block.yml'));
$field_block_field = Yaml::decode(file_get_contents(__DIR__ . '/field.field.node.page.field_block.yml'));

// Add the field storage to the config table.
$connection->insert('config')
  ->fields([
    'collection',
    'name',
    'data',
  ])
  ->values([
    'collection' => '',
    'name' => 'field.storage.' . $field_block_storage['id'],
    'data' => serialize($field_block_storage),
  ])
  ->execute();

// We need to Update the registry of 'last installed' field definitions.
$installed = $connection->select('key_value')
  ->fields('key_value', ['value'])
  ->condition('collection', 'entity.definitions.installed')
  ->condition('name', 'node.field_storage_definitions')
  ->execute()
  ->fetchField();
$installed = unserialize($installed);
$installed['field_block'] = new FieldStorageConfig($field_block_storage);
$connection->update('key_value')
  ->condition('collection', 'entity.definitions.installed')
  ->condition('name', 'node.field_storage_definitions')
  ->fields([
    'value' => serialize($installed),
  ])
  ->execute();

// Add the entity_storage_schema.
$connection->insert('key_value')
  ->fields([
    'collection',
    'name',
    'value',
  ])->values([
    'collection' => 'entity.storage_schema.sql',
    'name' => 'node.field_schema_data.field_block',
    'value' => 'a:2:{s:17:"node__field_block";a:4:{s:11:"description";s:40:"Data storage for node field field_block.";s:6:"fields";a:7:{s:6:"bundle";a:5:{s:4:"type";s:13:"varchar_ascii";s:6:"length";i:128;s:8:"not null";b:1;s:7:"default";s:0:"";s:11:"description";s:88:"The field instance bundle to which this row belongs, used when deleting a field instance";}s:7:"deleted";a:5:{s:4:"type";s:3:"int";s:4:"size";s:4:"tiny";s:8:"not null";b:1;s:7:"default";i:0;s:11:"description";s:60:"A boolean indicating whether this data item has been deleted";}s:9:"entity_id";a:4:{s:4:"type";s:3:"int";s:8:"unsigned";b:1;s:8:"not null";b:1;s:11:"description";s:38:"The entity id this data is attached to";}s:11:"revision_id";a:4:{s:4:"type";s:3:"int";s:8:"unsigned";b:1;s:8:"not null";b:1;s:11:"description";s:47:"The entity revision id this data is attached to";}s:8:"langcode";a:5:{s:4:"type";s:13:"varchar_ascii";s:6:"length";i:32;s:8:"not null";b:1;s:7:"default";s:0:"";s:11:"description";s:37:"The language code for this data item.";}s:5:"delta";a:4:{s:4:"type";s:3:"int";s:8:"unsigned";b:1;s:8:"not null";b:1;s:11:"description";s:67:"The sequence number for this data item, used for multi-value fields";}s:17:"field_block_value";a:2:{s:4:"type";s:7:"varchar";s:6:"length";i:255;}}s:11:"primary key";a:4:{i:0;s:9:"entity_id";i:1;s:7:"deleted";i:2;s:5:"delta";i:3;s:8:"langcode";}s:7:"indexes";a:2:{s:6:"bundle";a:1:{i:0;s:6:"bundle";}s:11:"revision_id";a:1:{i:0;s:11:"revision_id";}}}s:26:"node_revision__field_block";a:4:{s:11:"description";s:52:"Revision archive storage for node field field_block.";s:6:"fields";a:7:{s:6:"bundle";a:5:{s:4:"type";s:13:"varchar_ascii";s:6:"length";i:128;s:8:"not null";b:1;s:7:"default";s:0:"";s:11:"description";s:88:"The field instance bundle to which this row belongs, used when deleting a field instance";}s:7:"deleted";a:5:{s:4:"type";s:3:"int";s:4:"size";s:4:"tiny";s:8:"not null";b:1;s:7:"default";i:0;s:11:"description";s:60:"A boolean indicating whether this data item has been deleted";}s:9:"entity_id";a:4:{s:4:"type";s:3:"int";s:8:"unsigned";b:1;s:8:"not null";b:1;s:11:"description";s:38:"The entity id this data is attached to";}s:11:"revision_id";a:4:{s:4:"type";s:3:"int";s:8:"unsigned";b:1;s:8:"not null";b:1;s:11:"description";s:47:"The entity revision id this data is attached to";}s:8:"langcode";a:5:{s:4:"type";s:13:"varchar_ascii";s:6:"length";i:32;s:8:"not null";b:1;s:7:"default";s:0:"";s:11:"description";s:37:"The language code for this data item.";}s:5:"delta";a:4:{s:4:"type";s:3:"int";s:8:"unsigned";b:1;s:8:"not null";b:1;s:11:"description";s:67:"The sequence number for this data item, used for multi-value fields";}s:17:"field_block_value";a:2:{s:4:"type";s:7:"varchar";s:6:"length";i:255;}}s:11:"primary key";a:5:{i:0;s:9:"entity_id";i:1;s:11:"revision_id";i:2;s:7:"deleted";i:3;s:5:"delta";i:4;s:8:"langcode";}s:7:"indexes";a:2:{s:6:"bundle";a:1:{i:0;s:6:"bundle";}s:11:"revision_id";a:1:{i:0;s:11:"revision_id";}}}}',
  ])->execute();

$connection->insert('config')
  ->fields([
    'collection',
    'name',
    'data',
  ])->values([
    'collection' => '',
    'name' => 'field.field.' . $field_block_field['id'],
    'data' => serialize($field_block_field),
  ])->execute();

// Update entity.definitions.bundle_field_map
$value = $connection->select('key_value')
  ->fields('key_value', ['value'])
  ->condition('collection', 'entity.definitions.bundle_field_map')
  ->condition('name', 'node')
  ->execute()
  ->fetchField();

$value = unserialize($value);
$value['field_block'] = array('type' => 'plugin_reference', 'bundles' => array('page' => 'page'));

$connection->update('key_value')
  ->fields([
    'value' => serialize($value),
  ])
  ->condition('collection', 'entity.definitions.bundle_field_map')
  ->condition('name', 'node')
  ->execute();

// Add entity form display config.
$connection->update('config')
  ->fields([
    'data' => 'a:10:{s:4:"uuid";s:36:"bf1a5442-e4fb-437c-8c6a-4518deb6052f";s:8:"langcode";s:2:"en";s:6:"status";b:1;s:12:"dependencies";a:2:{s:6:"config";a:3:{i:0;s:26:"field.field.node.page.body";i:1;s:33:"field.field.node.page.field_block";i:2;s:14:"node.type.page";}s:6:"module";a:2:{i:0;s:4:"path";i:1;s:15:"pluginreference";}}s:2:"id";s:17:"node.page.default";s:16:"targetEntityType";s:4:"node";s:6:"bundle";s:4:"page";s:4:"mode";s:7:"default";s:7:"content";a:8:{s:7:"created";a:5:{s:4:"type";s:18:"datetime_timestamp";s:6:"weight";i:2;s:6:"region";s:7:"content";s:8:"settings";a:0:{}s:20:"third_party_settings";a:0:{}}s:11:"field_block";a:5:{s:4:"type";s:29:"plugin_reference_autocomplete";s:6:"weight";i:7;s:6:"region";s:7:"content";s:8:"settings";a:1:{s:17:"provider_grouping";b:1;}s:20:"third_party_settings";a:0:{}}s:4:"path";a:5:{s:4:"type";s:4:"path";s:6:"weight";i:5;s:6:"region";s:7:"content";s:8:"settings";a:0:{}s:20:"third_party_settings";a:0:{}}s:7:"promote";a:5:{s:4:"type";s:16:"boolean_checkbox";s:8:"settings";a:1:{s:13:"display_label";b:1;}s:6:"weight";i:3;s:6:"region";s:7:"content";s:20:"third_party_settings";a:0:{}}s:6:"status";a:5:{s:4:"type";s:16:"boolean_checkbox";s:8:"settings";a:1:{s:13:"display_label";b:1;}s:6:"weight";i:6;s:6:"region";s:7:"content";s:20:"third_party_settings";a:0:{}}s:6:"sticky";a:5:{s:4:"type";s:16:"boolean_checkbox";s:8:"settings";a:1:{s:13:"display_label";b:1;}s:6:"weight";i:4;s:6:"region";s:7:"content";s:20:"third_party_settings";a:0:{}}s:5:"title";a:5:{s:4:"type";s:16:"string_textfield";s:6:"weight";i:0;s:6:"region";s:7:"content";s:8:"settings";a:2:{s:4:"size";i:60;s:11:"placeholder";s:0:"";}s:20:"third_party_settings";a:0:{}}s:3:"uid";a:5:{s:4:"type";s:29:"entity_reference_autocomplete";s:6:"weight";i:1;s:8:"settings";a:4:{s:14:"match_operator";s:8:"CONTAINS";s:4:"size";i:60;s:11:"placeholder";s:0:"";s:11:"match_limit";i:10;}s:6:"region";s:7:"content";s:20:"third_party_settings";a:0:{}}}s:6:"hidden";a:1:{s:4:"body";b:1;}}',
  ])
  ->condition('collection', '')
  ->condition('name', 'core.entity_form_display.node.page.default')
  ->execute();
$connection->update('key_value')
  ->fields([
    'value' => 'a:1:{i:0;s:42:"core.entity_form_display.node.page.default";}']
  )
  ->condition('collection', 'config.entity.key_store.entity_form_display')
  ->condition('name', 'uuid:bf1a5442-e4fb-437c-8c6a-4518deb6052f')
  ->execute();

// Add an installed plugin_reference field with dummy data.
$connection->schema()->createTable('node__field_block', array(
  'fields' => array(
    'bundle' => array(
      'type' => 'varchar_ascii',
      'not null' => TRUE,
      'length' => '128',
      'default' => '',
    ),
    'deleted' => array(
      'type' => 'int',
      'not null' => TRUE,
      'size' => 'tiny',
      'default' => '0',
    ),
    'entity_id' => array(
      'type' => 'int',
      'not null' => TRUE,
      'size' => 'normal',
      'unsigned' => TRUE,
    ),
    'revision_id' => array(
      'type' => 'int',
      'not null' => TRUE,
      'size' => 'normal',
      'unsigned' => TRUE,
    ),
    'langcode' => array(
      'type' => 'varchar_ascii',
      'not null' => TRUE,
      'length' => '32',
      'default' => '',
    ),
    'delta' => array(
      'type' => 'int',
      'not null' => TRUE,
      'size' => 'normal',
      'unsigned' => TRUE,
    ),
    'field_block_value' => array(
      'type' => 'varchar',
      'not null' => TRUE,
      'length' => '255',
    ),
  ),
  'primary key' => array(
    'entity_id',
    'deleted',
    'delta',
    'langcode',
  ),
  'indexes' => array(
    'bundle' => array(
      'bundle',
    ),
    'revision_id' => array(
      'revision_id',
    ),
  ),
  'mysql_character_set' => 'utf8mb4',
));

$connection->schema()->createTable('node_revision__field_block', array(
  'fields' => array(
    'bundle' => array(
      'type' => 'varchar_ascii',
      'not null' => TRUE,
      'length' => '128',
      'default' => '',
    ),
    'deleted' => array(
      'type' => 'int',
      'not null' => TRUE,
      'size' => 'tiny',
      'default' => '0',
    ),
    'entity_id' => array(
      'type' => 'int',
      'not null' => TRUE,
      'size' => 'normal',
      'unsigned' => TRUE,
    ),
    'revision_id' => array(
      'type' => 'int',
      'not null' => TRUE,
      'size' => 'normal',
      'unsigned' => TRUE,
    ),
    'langcode' => array(
      'type' => 'varchar_ascii',
      'not null' => TRUE,
      'length' => '32',
      'default' => '',
    ),
    'delta' => array(
      'type' => 'int',
      'not null' => TRUE,
      'size' => 'normal',
      'unsigned' => TRUE,
    ),
    'field_block_value' => array(
      'type' => 'varchar',
      'not null' => TRUE,
      'length' => '255',
    ),
  ),
  'primary key' => array(
    'entity_id',
    'revision_id',
    'deleted',
    'delta',
    'langcode',
  ),
  'indexes' => array(
    'bundle' => array(
      'bundle',
    ),
    'revision_id' => array(
      'revision_id',
    ),
  ),
  'mysql_character_set' => 'utf8mb4',
));
$connection->insert('node')
  ->fields(array(
    'nid',
    'vid',
    'type',
    'uuid',
    'langcode',
  ))
  ->values(array(
    'nid' => '1',
    'vid' => '1',
    'type' => 'page',
    'uuid' => '601cc17b-e572-4c36-9cba-5a35f9295ef2',
    'langcode' => 'en',
  ))
  ->execute();
$connection->insert('node_field_data')
  ->fields(array(
    'nid',
    'vid',
    'type',
    'title',
    'created',
    'changed',
    'promote',
    'sticky',
    'revision_translation_affected',
    'default_langcode',
    'langcode',
    'status',
    'uid',
  ))
  ->values(array(
    'nid' => '1',
    'vid' => '1',
    'type' => 'page',
    'title' => 'Test pluginreference',
    'created' => '1612458797',
    'changed' => '1612458817',
    'promote' => '0',
    'sticky' => '0',
    'revision_translation_affected' => '1',
    'default_langcode' => '1',
    'langcode' => 'en',
    'status' => '1',
    'uid' => '1',
  ))
  ->execute();

$connection->insert('node_revision')
  ->fields(array(
    'nid',
    'vid',
    'revision_timestamp',
    'revision_uid',
    'revision_log',
    'revision_default',
    'langcode',
  ))
  ->values(array(
    'nid' => '1',
    'vid' => '1',
    'revision_timestamp' => '1612458817',
    'revision_uid' => '1',
    'revision_log' => NULL,
    'revision_default' => '1',
    'langcode' => 'en',
  ))
  ->execute();

$connection->insert('node_field_revision')
  ->fields(array(
    'nid',
    'vid',
    'title',
    'created',
    'changed',
    'promote',
    'sticky',
    'revision_translation_affected',
    'default_langcode',
    'langcode',
    'status',
    'uid',
  ))
  ->values(array(
    'nid' => '1',
    'vid' => '1',
    'title' => 'Test pluginreference',
    'created' => '1612458797',
    'changed' => '1612458966',
    'promote' => '0',
    'sticky' => '0',
    'revision_translation_affected' => '1',
    'default_langcode' => '1',
    'langcode' => 'en',
    'status' => '1',
    'uid' => '1',
  ))
  ->execute();

$connection->insert('node__field_block')
  ->fields(array(
    'bundle',
    'deleted',
    'entity_id',
    'revision_id',
    'langcode',
    'delta',
    'field_block_value',
  ))
  ->values(array(
    'bundle' => 'page',
    'deleted' => '0',
    'entity_id' => '1',
    'revision_id' => '1',
    'langcode' => 'en',
    'delta' => '0',
    'field_block_value' => 'page_title_block',
  ))->execute();
$connection->insert('node_revision__field_block')
  ->fields(array(
    'bundle',
    'deleted',
    'entity_id',
    'revision_id',
    'langcode',
    'delta',
    'field_block_value',
  ))
  ->values(array(
    'bundle' => 'page',
    'deleted' => '0',
    'entity_id' => '1',
    'revision_id' => '1',
    'langcode' => 'en',
    'delta' => '0',
    'field_block_value' => 'page_title_block',
  ))
  ->execute();
