<?php

namespace Drupal\Tests\pluginreference\Unit;

use Drupal\Component\DependencyInjection\Container;
use Drupal\Core\Block\BlockManager;
use Drupal\Core\Block\BlockManagerInterface;
use Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManager;
use Drupal\Core\Entity\EntityReferenceSelection\SelectionPluginManagerInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Field\WidgetPluginManager;
use Drupal\pluginreference\PluginReferenceSelectionManager;
use Drupal\pluginreference\PluginTypeHelper;
use Drupal\pluginreference_test\Plugin\Block\PluginReferenceTestAccessBlock;
use Drupal\Tests\UnitTestCase;
use Drupal\text\Plugin\Field\FieldWidget\TextfieldWidget;

/**
 * @coversDefaultClass \Drupal\pluginreference\PluginTypeHelper
 * @group pluginreference
 */
class PluginTypeHelperTest extends UnitTestCase {

  /**
   * The plugin type helper.
   *
   * @var \Drupal\pluginreference\PluginTypeHelper
   */
  protected $pluginTypeHelper;

  /**
   * Plugin manager services with their corresponding class used in the tests.
   *
   * @var string[]
   */
  public static $pluginManagerServices = [
    'plugin.manager.block' => BlockManager::class,
    'plugin.manager.entity_reference_selection' => SelectionPluginManager::class,
    'plugin.manager.field.widget' => WidgetPluginManager::class,
    'plugin.manager.plugin_reference_selection' => PluginReferenceSelectionManager::class,
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $container = $this->createMock(Container::class);
    $container
      ->method('getServiceIds')
      ->willReturn([
        'plugin.manager.block',
        'plugin.manager.entity_reference_selection',
        'plugin.manager.field.widget',
        'plugin.manager.plugin_reference_selection',
        'entity_type.manager',
      ]);
    $container
      ->method('has')
      ->willReturnCallback(function (string $service_id) {
        return isset(self::$pluginManagerServices[$service_id]);
      });
    $container
      ->method('get')
      ->willReturnCallback(function (string $service_id) {
        $plugin_manager_service = isset(self::$pluginManagerServices[$service_id]) ? $this->createMock(self::$pluginManagerServices[$service_id]) : NULL;

        if ($plugin_manager_service !== NULL) {
          switch ($service_id) {
            case 'plugin.manager.block':
              $provider = 'block';
              break;

            case 'plugin.manager.plugin_reference_selection':
              $provider = 'pluginreference';
              break;

            default:
              $provider = 'system';
          }

          $return_value = [
            'plugin_1' => [
              'label' => 'Plugin 1',
              'provider' => $provider,
            ],
            'plugin_2' => [
              'label' => 'Plugin 2',
              'provider' => $provider,
            ],
          ];

          $plugin_manager_service
            ->method('getDefinitions')
            ->willReturn($return_value);
        }

        return $plugin_manager_service;
      });
    $module_extension_list = $this->createMock(ModuleExtensionList::class);
    $module_extension_list
      ->method('getName')
      ->willReturnCallback(function (string $module) {
        $module_names = [
          'system' => 'System',
          'pluginreference' => 'Plugin Reference',
          'block' => 'Block',
        ];
        return $module_names[$module] ?? NULL;
      });

    $this->pluginTypeHelper = new PluginTypeHelper($container, $module_extension_list);
  }

  /**
   * Tests the ::getPluginTypeIds method.
   *
   * @covers ::getPluginTypeIds
   */
  public function testGetPluginTypeIds() {
    $this->assertEquals([
      'block',
      'entity_reference_selection',
      'field.widget',
      'plugin_reference_selection',
    ],
      $this->pluginTypeHelper->getPluginTypeIds());
  }

  /**
   * Tests the ::pluginTypeExists method.
   *
   * @dataProvider pluginTypeExistsDataProvider
   *
   * @covers ::pluginTypeExists
   */
  public function testPluginTypeExists(bool $expected, string $plugin_type_id) {
    $this->assertEquals($expected, $this->pluginTypeHelper->pluginTypeExists($plugin_type_id));
  }

  /**
   * Data provider for testPluginTypeExists().
   *
   * @see testPluginTypeExists()
   */
  public static function pluginTypeExistsDataProvider() {
    return [
      [
        TRUE,
        'block',
      ],
      [
        TRUE,
        'entity_reference_selection',
      ],
      [
        TRUE,
        'field.widget',
      ],
      [
        FALSE,
        'image.style',
      ],
    ];
  }

  /**
   * Tests the ::getPluginTypeName method.
   *
   * @dataProvider getPluginTypeNameDataProvider
   *
   * @covers ::getPluginTypeName
   */
  public function testGetPluginTypeName(string $expected, string $plugin_type_id) {
    $this->assertEquals($expected, $this->pluginTypeHelper->getPluginTypeName($plugin_type_id));
  }

  /**
   * Data provider for testGetPluginTypeName().
   *
   * @see testGetPluginTypeName()
   */
  public static function getPluginTypeNameDataProvider() {
    return [
      [
        'Block',
        'block',
      ],
      [
        'Entity Reference Selection',
        'entity_reference_selection',
      ],
      [
        'Field Widget',
        'field.widget',
      ],
    ];
  }

  /**
   * Tests the ::getPluginTypeProvider method.
   *
   * @dataProvider getPluginTypeProviderDataProvider
   *
   * @covers ::getPluginTypeProvider
   */
  public function testGetPluginTypeProvider(?string $expected, string $plugin_type_id) {
    $this->assertEquals($expected, $this->pluginTypeHelper->getPluginTypeProvider($plugin_type_id));
  }

  /**
   * Data provider for testGetPluginTypeProvider().
   *
   * @see testGetPluginTypeProvider()
   */
  public static function getPluginTypeProviderDataProvider() {
    return [
      [
        'system',
        'block',
      ],
      [
        'system',
        'field.widget',
      ],
      [
        'pluginreference',
        'plugin_reference_selection',
      ],
      [
        NULL,
        'nonexistent_plugin_type',
      ],
    ];
  }

  /**
   * Tests the ::getPluginTypeProviderName method.
   *
   * @dataProvider getPluginTypeProviderNameDataProvider
   *
   * @covers ::getPluginTypeProviderName
   */
  public function testGetPluginTypeProviderName(?string $expected, string $plugin_type_id) {
    $this->assertEquals($expected, $this->pluginTypeHelper->getPluginTypeProviderName($plugin_type_id));
  }

  /**
   * Data provider for testGetPluginTypeProviderName().
   *
   * @see testGetPluginTypeProviderName()
   */
  public static function getPluginTypeProviderNameDataProvider() {
    return [
      [
        'System',
        'block',
      ],
      [
        'System',
        'field.widget',
      ],
      [
        'Plugin Reference',
        'plugin_reference_selection',
      ],
      [
        NULL,
        'nonexistent_plugin_type',
      ],
    ];
  }

  /**
   * Tests the ::getProviderName method.
   *
   * @dataProvider getProviderNameDataProvider
   *
   * @covers ::getProviderName
   */
  public function testGetProviderName(string $expected, string $provider) {
    $this->assertEquals($expected, $this->pluginTypeHelper->getProviderName($provider));
  }

  /**
   * Data provider for testGetProviderName().
   *
   * @see testGetProviderName()
   */
  public static function getProviderNameDataProvider() {
    return [
      [
        'Plugin Reference',
        'pluginreference',
      ],
      [
        'Block',
        'block',
      ],
      [
        'System',
        'system',
      ],
    ];
  }

  /**
   * Tests the ::getPluginTypeOptions method.
   *
   * @covers ::getPluginTypeOptions
   */
  public function testGetPluginTypeOptions() {
    $this->assertEquals([
      'Plugin Reference' => [
        'plugin_reference_selection' => 'Plugin Reference Selection',
      ],
      'System' => [
        'block' => 'Block',
        'entity_reference_selection' => 'Entity Reference Selection',
        'field.widget' => 'Field Widget',
      ],
    ], $this->pluginTypeHelper->getPluginTypeOptions());
  }

  /**
   * Tests the ::getPluginDefinitions method.
   *
   * @dataProvider getPluginDefinitionsDataProvider
   *
   * @covers ::getPluginDefinitions
   */
  public function testGetPluginDefinitions(array $expected, string $plugin_type_id) {
    $this->assertEquals($expected, $this->pluginTypeHelper->getPluginDefinitions($plugin_type_id));
  }

  /**
   * Data provider for testGetPluginDefinitions().
   *
   * @see testGetPluginDefinitions()
   */
  public static function getPluginDefinitionsDataProvider() {
    return [
      [
        [
          'plugin_1' => [
            'label' => 'Plugin 1',
            'provider' => 'block',
          ],
          'plugin_2' => [
            'label' => 'Plugin 2',
            'provider' => 'block',
          ],
        ],
        'block',
      ],
      [
        [
          'plugin_1' => [
            'label' => 'Plugin 1',
            'provider' => 'pluginreference',
          ],
          'plugin_2' => [
            'label' => 'Plugin 2',
            'provider' => 'pluginreference',
          ],
        ],
        'plugin_reference_selection',
      ],
      [
        [
          'plugin_1' => [
            'label' => 'Plugin 1',
            'provider' => 'system',
          ],
          'plugin_2' => [
            'label' => 'Plugin 2',
            'provider' => 'system',
          ],
        ],
        'field.widget',
      ],
      [
        [],
        'nonexistent_plugin',
      ],
    ];
  }

  /**
   * Tests the ::getPluginLabel method.
   *
   * @dataProvider getPluginLabelDataProvider
   *
   * @covers ::getPluginLabel
   */
  public function testGetPluginLabel(string $expected, array $plugin_definition) {
    $this->assertEquals($expected, $this->pluginTypeHelper->getPluginLabel($plugin_definition));
  }

  /**
   * Data provider for testGetPluginLabel().
   *
   * @see testGetPluginLabel()
   */
  public static function getPluginLabelDataProvider() {
    return [
      [
        'Label 1',
        [
          'label' => 'Label 1',
        ],
      ],
      [
        'Label 2',
        [
          'admin_label' => 'Label 2',
        ],
      ],
      [
        'plugin_id',
        [
          'id' => 'plugin_id',
        ],
      ],
      [
        'Label 3',
        [
          'label' => 'Label 3',
          'admin_label' => 'Label 4',
          'id' => 'plugin_id',
        ],
      ],
      [
        'Label 5',
        [
          'admin_label' => 'Label 5',
          'id' => 'plugin_id',
        ],
      ],
    ];
  }

  /**
   * Tests the ::hasPluginAccessControl method.
   *
   * @dataProvider hasPluginAccessControlDataProvider
   *
   * @covers ::hasPluginAccessControl
   */
  public function testHasPluginAccessControl(bool $expected, array $plugin_definition) {
    $this->assertEquals($expected, $this->pluginTypeHelper->hasPluginAccessControl($plugin_definition));
  }

  /**
   * Data provider for testHasPluginAccessControl().
   *
   * @see testHasPluginAccessControl()
   */
  public static function hasPluginAccessControlDataProvider() {
    return [
      [
        TRUE,
        [
          'class' => PluginReferenceTestAccessBlock::class,
        ],
      ],
      [
        FALSE,
        [],
      ],
      [
        FALSE,
        [
          'class' => TextfieldWidget::class,
        ],
      ],
    ];
  }

  /**
   * Tests the ::isPluginCacheable method.
   *
   * @dataProvider isPluginCacheableDataProvider
   *
   * @covers ::isPluginCacheable
   */
  public function testIsPluginCacheable(bool $expected, array $plugin_definition) {
    $this->assertEquals($expected, $this->pluginTypeHelper->isPluginCacheable($plugin_definition));
  }

  /**
   * Data provider for testIsPluginCacheable().
   *
   * @see testIsPluginCacheable()
   */
  public static function isPluginCacheableDataProvider() {
    return [
      [
        TRUE,
        [
          'class' => PluginReferenceTestAccessBlock::class,
        ],
      ],
      [
        FALSE,
        [],
      ],
      [
        FALSE,
        [
          'class' => TextfieldWidget::class,
        ],
      ],
    ];
  }

  /**
   * Tests the ::isPluginConfigurable method.
   *
   * @dataProvider isPluginConfigurableDataProvider
   *
   * @covers ::isPluginConfigurable
   */
  public function testIsPluginConfigurable(bool $expected, array $plugin_definition) {
    $this->assertEquals($expected, $this->pluginTypeHelper->isPluginConfigurable($plugin_definition));
  }

  /**
   * Data provider for testIsPluginConfigurable().
   *
   * @see testIsPluginConfigurable()
   */
  public static function isPluginConfigurableDataProvider() {
    return [
      [
        TRUE,
        [
          'class' => PluginReferenceTestAccessBlock::class,
        ],
      ],
      [
        FALSE,
        [],
      ],
      [
        FALSE,
        [
          'class' => TextfieldWidget::class,
        ],
      ],
    ];
  }

  /**
   * Tests the ::getPluginManager method.
   *
   * @dataProvider getPluginManagerDataProvider
   *
   * @covers ::getPluginManager
   */
  public function testGetPluginManager(?string $expected, string $plugin_type_id) {
    if ($expected === NULL) {
      $this->assertEmpty($this->pluginTypeHelper->getPluginManager($plugin_type_id));
    }
    else {
      $this->assertInstanceOf($expected, $this->pluginTypeHelper->getPluginManager($plugin_type_id));
    }
  }

  /**
   * Data provider for testGetPluginManager().
   *
   * @see testGetPluginManager()
   */
  public static function getPluginManagerDataProvider() {
    return [
      [
        BlockManagerInterface::class,
        'block',
      ],
      [
        SelectionPluginManagerInterface::class,
        'entity_reference_selection',
      ],
      [
        WidgetPluginManager::class,
        'field.widget',
      ],
      [
        NULL,
        'random',
      ],
    ];
  }

}

namespace Drupal\pluginreference;

use Drupal\Tests\pluginreference\Unit\PluginTypeHelperTest;

/**
 * Shadow get_class() system call.
 *
 * @returns string
 */
function get_class($class) {
  foreach (PluginTypeHelperTest::$pluginManagerServices as $pluginManagerService) {
    if ($class instanceof $pluginManagerService) {
      return $pluginManagerService;
    }
  }

  return \get_class($class);
}
