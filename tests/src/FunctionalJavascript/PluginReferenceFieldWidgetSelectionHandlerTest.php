<?php

namespace Drupal\Tests\pluginreference\FunctionalJavascript;

use Drupal\field\Entity\FieldConfig;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\Tests\pluginreference\Traits\PluginReferenceTrait;

/**
 * Test that configuring a selection handler works.
 *
 * @group pluginreference
 */
class PluginReferenceFieldWidgetSelectionHandlerTest extends WebDriverTestBase {

  use PluginReferenceTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'node',
    'field_ui',
    'pluginreference_test',
    'system',
  ];

  /**
   * The node type.
   *
   * @var \Drupal\node\Entity\NodeType
   */
  protected $nodeType;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->drupalLogin($this->rootUser);
    $this->nodeType = $this->drupalCreateContentType();
  }

  /**
   * Test the selection handler settings form.
   */
  public function testSelectionHandlerSettingsForm() {
    $assert_session = $this->assertSession();
    $page = $this->getSession()->getPage();

    // Add a new field.
    $this->drupalGet(sprintf('admin/structure/types/manage/%s/fields/add-field', $this->nodeType->id()));

    // Create a plugin reference field that references field widgets.
    $page->findField('new_storage_type')->setValue('plugin_reference');
    $page->pressButton('Continue');
    $assert_session->waitForField('label')->setValue('Widget reference');
    $machine_name = $assert_session->waitForElement('xpath', '//*[@id="edit-label-machine-name-suffix"]/span[contains(text(), "field_widget_reference")]');
    $this->assertNotEmpty($machine_name);
    $page->findField('group_field_options_wrapper')->setValue('field_ui:plugin_reference:field.widget');
    $page->pressButton('Continue');

    // Check the default settings.
    $this->assertEquals('default:field.widget', $page->findField('settings[handler]')->getValue());
    $this->assertEquals('label', $page->findField('settings[handler_settings][sort][key]')->getValue());
    $this->assertEquals('ASC', $page->findField('settings[handler_settings][sort][direction]')->getValue());
    $page->pressButton('Save settings');

    // Check that the handler and handler_settings are correctly saved.
    $field_config = FieldConfig::loadByName('node', $this->nodeType->id(), 'field_widget_reference');
    $this->assertEquals('default:field.widget', $field_config->getSetting('handler'));
    $this->assertEquals([
      'sort' => [
        'key' => 'label',
        'direction' => 'ASC',
      ],
    ], $field_config->getSetting('handler_settings'));

    // Update the handler and handler settings.
    $this->drupalGet(sprintf('admin/structure/types/manage/%s/fields/node.%s.field_widget_reference', $this->nodeType->id(), $this->nodeType->id()));

    $page->findField('settings[handler]')->setValue('field_widget_advanced');
    $assert_session->assertNoElementAfterWait('css', '.ajax-progress');
    $page->findField('settings[handler_settings][sort][key]')->setValue('id');
    $page->findField('settings[handler_settings][sort][direction]')->setValue('DESC');
    $page->pressButton('Save settings');

    // Check that the handler and handler_settings are correctly saved.
    $field_config = FieldConfig::loadByName('node', $this->nodeType->id(), 'field_widget_reference');
    $this->assertEquals('field_widget_advanced', $field_config->getSetting('handler'));
    $this->assertEquals([
      'sort' => [
        'key' => 'id',
        'direction' => 'DESC',
      ],
    ], $field_config->getSetting('handler_settings'));

    // Update the handler and handler settings.
    $this->drupalGet(sprintf('admin/structure/types/manage/%s/fields/node.%s.field_widget_reference', $this->nodeType->id(), $this->nodeType->id()));
    $page->findField('settings[handler]')->setValue('filtered:field.widget');
    $assert_session->assertNoElementAfterWait('css', '.ajax-progress');

    $page->findField('settings[handler_settings][filter][key]')->setValue('id');
    $page->findField('settings[handler_settings][filter][negate]')->setValue(1);
    $page->findField('settings[handler_settings][filter][target_values][plugin_reference_autocomplete]')->check();
    $page->findField('settings[handler_settings][filter][target_values][boolean_checkbox]')->check();

    $page->findField('settings[handler_settings][sort][key]')->setValue('id');
    $page->findField('settings[handler_settings][sort][direction]')->setValue('DESC');

    $page->pressButton('Save settings');
    $field_config = FieldConfig::loadByName('node', $this->nodeType->id(), 'field_widget_reference');
    $this->assertEquals('filtered:field.widget', $field_config->getSetting('handler'));
    $this->assertEquals([
      'filter' => [
        'key' => 'id',
        'negate' => TRUE,
        'target_values' => [
          'plugin_reference_autocomplete' => 'plugin_reference_autocomplete',
          'boolean_checkbox' => 'boolean_checkbox',
        ],
      ],
      'sort' => [
        'key' => 'id',
        'direction' => 'DESC',
      ],
    ], $field_config->getSetting('handler_settings'));

    // Check that the provider option is available.
    $this->drupalGet(sprintf('admin/structure/types/manage/%s/fields/node.%s.field_widget_reference', $this->nodeType->id(), $this->nodeType->id()));
    $page->findField('settings[handler_settings][filter][key]')->setValue('provider');
    // The category option is only available for blocks.
    $this->assertSession()->optionNotExists('settings[handler_settings][filter][key]', 'category');
    $assert_session->assertNoElementAfterWait('css', '.ajax-progress');
    $page->findField('settings[handler_settings][filter][negate]')->setValue(0);
    $page->findField('settings[handler_settings][filter][target_values][pluginreference]')->check();
    $page->pressButton('Save settings');

    $field_config = FieldConfig::loadByName('node', $this->nodeType->id(), 'field_widget_reference');
    $this->assertEquals('filtered:field.widget', $field_config->getSetting('handler'));
    $this->assertEquals([
      'filter' => [
        'key' => 'provider',
        'negate' => FALSE,
        'target_values' => [
          'pluginreference' => 'pluginreference',
        ],
      ],
      'sort' => [
        'key' => 'id',
        'direction' => 'DESC',
      ],
    ], $field_config->getSetting('handler_settings'));

    // Add a new field.
    $this->drupalGet(sprintf('admin/structure/types/manage/%s/fields/add-field', $this->nodeType->id()));

    // Create a plugin reference field that references blocks.
    $page->findField('new_storage_type')->setValue('plugin_reference');
    $page->pressButton('Continue');
    $assert_session->waitForField('label')->setValue('Block reference');
    $machine_name = $assert_session->waitForElement('xpath', '//*[@id="edit-label-machine-name-suffix"]/span[contains(text(), "field_block_reference")]');
    $this->assertNotEmpty($machine_name);
    $page->findField('group_field_options_wrapper')->setValue('field_ui:plugin_reference:block');
    $page->pressButton('Continue');

    $page->findField('settings[handler]')->setValue('filtered:block');
    $assert_session->assertNoElementAfterWait('css', '.ajax-progress');

    // Check that the category filter is present.
    $page->findField('settings[handler_settings][filter][key]')->setValue('category');
    $assert_session->assertNoElementAfterWait('css', '.ajax-progress');
    $page->findField('settings[handler_settings][filter][target_values][Menus]')->check();
    $page->pressButton('Save settings');

    $field_config = FieldConfig::loadByName('node', $this->nodeType->id(), 'field_block_reference');
    $this->assertEquals('filtered:block', $field_config->getSetting('handler'));
    $this->assertEquals([
      'filter' => [
        'key' => 'category',
        'negate' => FALSE,
        'target_values' => [
          'Menus' => 'Menus',
        ],
      ],
      'sort' => [
        'key' => 'label',
        'direction' => 'ASC',
      ],
    ], $field_config->getSetting('handler_settings'));
  }

}
