<?php

namespace Drupal\Tests\pluginreference\FunctionalJavascript\PluginReferenceSelection;

use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\field\Entity\FieldConfig;

/**
 * Test the usage of the filtered selection in the UI.
 *
 * @group pluginreference
 */
class FilteredSelectionUiTest extends SelectionUiTestBase {

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $field_config = FieldConfig::loadByName('node', $this->nodeType->id(), $this->fieldName);
    $field_config
      ->setSetting('handler', 'filtered:' . $this->targetType)
      ->setSetting('handler_settings', [
        'filter' => [
          'key' => 'id',
          'negate' => FALSE,
          'target_values' => [
            'plugin_reference_test_access_block' => 'plugin_reference_test_access_block',
            'plugin_reference_test_block' => 'plugin_reference_test_block',
            'plugin_reference_test_caching_block' => 'plugin_reference_test_caching_block',
          ],
        ],
        'sort' => [
          'key' => 'label',
          'direction' => 'ASC',
        ],
      ])
      ->save();
  }

  /**
   * Test selection handler in the select widget.
   */
  public function testPluginReferenceSelectWidget() {
    $page = $this->getSession()->getPage();

    EntityFormDisplay::load(sprintf('node.%s.default', $this->nodeType->id()))
      ->setComponent($this->fieldName, [
        'type' => 'plugin_reference_select',
        'settings' => [
          'configuration_form' => 'hidden',
          'provider_grouping' => FALSE,
        ],
      ])->save();

    $this->drupalGet(sprintf('node/add/%s', $this->nodeType->id()));

    $plugin_id = $page->findField(sprintf('%s[0][plugin_id]', $this->fieldName));
    // Assert that only the selected plugins are available in the list.
    $this->assertSelectOptionsOrderEquals([
      'plugin_reference_test_access_block' => self::BLOCK_PLUGINS['plugin_reference_test_access_block'],
      'plugin_reference_test_block' => self::BLOCK_PLUGINS['plugin_reference_test_block'],
      'plugin_reference_test_caching_block' => self::BLOCK_PLUGINS['plugin_reference_test_caching_block'],
    ], $plugin_id);

    // Update the field config so the filtering is negated.
    /** @var \Drupal\field\FieldConfigInterface $field_config */
    $field_config = FieldConfig::loadByName('node', $this->nodeType->id(), $this->fieldName);
    $settings = $field_config->getSettings();
    $settings['handler_settings']['filter']['negate'] = TRUE;
    $field_config->setSettings($settings)->save();

    $this->drupalGet(sprintf('node/add/%s', $this->nodeType->id()));

    // Check that all plugins except the selected ones are in the list.
    $plugin_id = $page->findField(sprintf('%s[0][plugin_id]', $this->fieldName));
    $negated_block_plugins = self::BLOCK_PLUGINS;
    unset($negated_block_plugins['plugin_reference_test_access_block'], $negated_block_plugins['plugin_reference_test_block'], $negated_block_plugins['plugin_reference_test_caching_block']);
    $this->assertSelectOptionsOrderEquals($negated_block_plugins, $plugin_id);

    // Update the field config so the list is now filtered on provider.
    /** @var \Drupal\field\FieldConfigInterface $field_config */
    $field_config = FieldConfig::loadByName('node', $this->nodeType->id(), $this->fieldName);
    $settings = $field_config->getSettings();
    $settings['handler_settings']['filter'] = [
      'key' => 'provider',
      'negate' => FALSE,
      'target_values' => [
        'pluginreference_test' => 'pluginreference_test',
      ],
    ];
    $field_config->setSettings($settings)->save();

    $this->drupalGet(sprintf('node/add/%s', $this->nodeType->id()));

    $plugin_id = $page->findField(sprintf('%s[0][plugin_id]', $this->fieldName));
    // Assert that only the plugins of the chosen provider are available in the
    // list.
    $this->assertSelectOptionsOrderEquals([
      'plugin_reference_test_access_block' => self::BLOCK_PLUGINS['plugin_reference_test_access_block'],
      'plugin_reference_test_block' => self::BLOCK_PLUGINS['plugin_reference_test_block'],
      'plugin_reference_test_caching_block' => self::BLOCK_PLUGINS['plugin_reference_test_caching_block'],
    ], $plugin_id);

    // Update the field config so the list is now filtered on category.
    /** @var \Drupal\field\FieldConfigInterface $field_config */
    $field_config = FieldConfig::loadByName('node', $this->nodeType->id(), $this->fieldName);
    $settings = $field_config->getSettings();
    $settings['handler_settings']['filter'] = [
      'key' => 'category',
      'negate' => FALSE,
      'target_values' => [
        'Plugin Reference Test' => 'Plugin Reference Test',
      ],
    ];
    $field_config->setSettings($settings)->save();

    $this->drupalGet(sprintf('node/add/%s', $this->nodeType->id()));

    $plugin_id = $page->findField(sprintf('%s[0][plugin_id]', $this->fieldName));
    // Assert that only the plugins of the chosen category are available in the
    // list.
    $this->assertSelectOptionsOrderEquals([
      'plugin_reference_test_access_block' => self::BLOCK_PLUGINS['plugin_reference_test_access_block'],
      'plugin_reference_test_block' => self::BLOCK_PLUGINS['plugin_reference_test_block'],
      'plugin_reference_test_caching_block' => self::BLOCK_PLUGINS['plugin_reference_test_caching_block'],
    ], $plugin_id);
  }

  /**
   * Test selection handler in the autocomplete widget.
   */
  public function testPluginReferenceAutocompleteWidget() {
    EntityFormDisplay::load(sprintf('node.%s.default', $this->nodeType->id()))
      ->setComponent($this->fieldName, [
        'type' => 'plugin_reference_autocomplete',
        'settings' => ['configuration_form' => 'hidden'],
      ])->save();

    $this->drupalGet(sprintf('node/add/%s', $this->nodeType->id()));

    // Assert that only the selected plugins are available in the list.
    $this->assertAutocompleteOrderEquals([
      self::BLOCK_PLUGINS['plugin_reference_test_access_block'],
      self::BLOCK_PLUGINS['plugin_reference_test_block'],
      self::BLOCK_PLUGINS['plugin_reference_test_caching_block'],
    ], sprintf('%s[0][plugin_id]', $this->fieldName), 'test');

    // Update the field config so the filtering is negated.
    /** @var \Drupal\field\FieldConfigInterface $field_config */
    $field_config = FieldConfig::loadByName('node', $this->nodeType->id(), $this->fieldName);
    $settings = $field_config->getSettings();
    $settings['handler_settings']['filter']['negate'] = TRUE;
    $field_config->setSettings($settings)->save();

    $this->drupalGet(sprintf('node/add/%s', $this->nodeType->id()));

    // Check that all plugins except the selected ones are in the list.
    $this->assertAutocompleteOrderEquals([
      self::BLOCK_PLUGINS['system_menu_block:admin'],
      self::BLOCK_PLUGINS['system_breadcrumb_block'],
      self::BLOCK_PLUGINS['system_menu_block:footer'],
      self::BLOCK_PLUGINS['system_menu_block:main'],
      self::BLOCK_PLUGINS['system_main_block'],
      self::BLOCK_PLUGINS['system_messages_block'],
      self::BLOCK_PLUGINS['page_title_block'],
      self::BLOCK_PLUGINS['system_powered_by_block'],
      self::BLOCK_PLUGINS['local_actions_block'],
      self::BLOCK_PLUGINS['system_branding_block'],

    ], sprintf('%s[0][plugin_id]', $this->fieldName), 't');

    // Update the field config so the list is now filtered on provider.
    /** @var \Drupal\field\FieldConfigInterface $field_config */
    $field_config = FieldConfig::loadByName('node', $this->nodeType->id(), $this->fieldName);
    $settings = $field_config->getSettings();
    $settings['handler_settings']['filter'] = [
      'key' => 'provider',
      'negate' => FALSE,
      'target_values' => [
        'pluginreference_test' => 'pluginreference_test',
      ],
    ];
    $field_config->setSettings($settings)->save();

    $this->drupalGet(sprintf('node/add/%s', $this->nodeType->id()));
    // Assert that only the plugins of the chosen provider are available in the
    // list.
    $this->assertAutocompleteOrderEquals([
      self::BLOCK_PLUGINS['plugin_reference_test_access_block'],
      self::BLOCK_PLUGINS['plugin_reference_test_block'],
      self::BLOCK_PLUGINS['plugin_reference_test_caching_block'],

    ], sprintf('%s[0][plugin_id]', $this->fieldName), 't');

    // Update the field config so the list is now filtered on category.
    /** @var \Drupal\field\FieldConfigInterface $field_config */
    $field_config = FieldConfig::loadByName('node', $this->nodeType->id(), $this->fieldName);
    $settings = $field_config->getSettings();
    $settings['handler_settings']['filter'] = [
      'key' => 'category',
      'negate' => FALSE,
      'target_values' => [
        'Plugin Reference Test' => 'Plugin Reference Test',
      ],
    ];
    $field_config->setSettings($settings)->save();

    $this->drupalGet(sprintf('node/add/%s', $this->nodeType->id()));

    // Assert that only the plugins of the chosen category are available in the
    // list.
    $this->assertAutocompleteOrderEquals([
      self::BLOCK_PLUGINS['plugin_reference_test_access_block'],
      self::BLOCK_PLUGINS['plugin_reference_test_block'],
      self::BLOCK_PLUGINS['plugin_reference_test_caching_block'],

    ], sprintf('%s[0][plugin_id]', $this->fieldName), 't');
  }

  /**
   * Test selection handler in the options button widget.
   */
  public function testPluginReferenceOptionsButtonsWidget() {
    EntityFormDisplay::load(sprintf('node.%s.default', $this->nodeType->id()))
      ->setComponent($this->fieldName, [
        'type' => 'plugin_reference_options_buttons',
        'settings' => ['configuration_form' => 'hidden'],
      ])->save();

    $this->drupalGet(sprintf('node/add/%s', $this->nodeType->id()));

    // Assert that only the selected plugins are available in the list.
    $this->assertOptionButtonsOrderEquals([
      'plugin_reference_test_access_block' => self::BLOCK_PLUGINS['plugin_reference_test_access_block'],
      'plugin_reference_test_block' => self::BLOCK_PLUGINS['plugin_reference_test_block'],
      'plugin_reference_test_caching_block' => self::BLOCK_PLUGINS['plugin_reference_test_caching_block'],
    ], sprintf('%s[plugin_id]', $this->fieldName));

    // Update the field config so the filtering is negated.
    /** @var \Drupal\field\FieldConfigInterface $field_config */
    $field_config = FieldConfig::loadByName('node', $this->nodeType->id(), $this->fieldName);
    $settings = $field_config->getSettings();
    $settings['handler_settings']['filter']['negate'] = TRUE;
    $field_config->setSettings($settings)->save();

    $this->drupalGet(sprintf('node/add/%s', $this->nodeType->id()));

    // Check that all plugins except the selected ones are in the list.
    $negated_block_plugins = self::BLOCK_PLUGINS;
    unset($negated_block_plugins['plugin_reference_test_access_block'], $negated_block_plugins['plugin_reference_test_block'], $negated_block_plugins['plugin_reference_test_caching_block']);
    $this->assertOptionButtonsOrderEquals($negated_block_plugins, sprintf('%s[plugin_id]', $this->fieldName));

    // Update the field config so the list is now filtered on provider.
    /** @var \Drupal\field\FieldConfigInterface $field_config */
    $field_config = FieldConfig::loadByName('node', $this->nodeType->id(), $this->fieldName);
    $settings = $field_config->getSettings();
    $settings['handler_settings']['filter'] = [
      'key' => 'provider',
      'negate' => FALSE,
      'target_values' => [
        'pluginreference_test' => 'pluginreference_test',
      ],
    ];
    $field_config->setSettings($settings)->save();

    $this->drupalGet(sprintf('node/add/%s', $this->nodeType->id()));
    // Assert that only the plugins of the chosen provider are available in the
    // list.
    $this->assertOptionButtonsOrderEquals([
      'plugin_reference_test_access_block' => self::BLOCK_PLUGINS['plugin_reference_test_access_block'],
      'plugin_reference_test_block' => self::BLOCK_PLUGINS['plugin_reference_test_block'],
      'plugin_reference_test_caching_block' => self::BLOCK_PLUGINS['plugin_reference_test_caching_block'],
    ], sprintf('%s[plugin_id]', $this->fieldName));

    // Update the field config so the list is now filtered on category.
    /** @var \Drupal\field\FieldConfigInterface $field_config */
    $field_config = FieldConfig::loadByName('node', $this->nodeType->id(), $this->fieldName);
    $settings = $field_config->getSettings();
    $settings['handler_settings']['filter'] = [
      'key' => 'category',
      'negate' => FALSE,
      'target_values' => [
        'Plugin Reference Test' => 'Plugin Reference Test',
      ],
    ];
    $field_config->setSettings($settings)->save();

    $this->drupalGet(sprintf('node/add/%s', $this->nodeType->id()));

    // Assert that only the plugins of the chosen category are available in the
    // list.
    $this->assertOptionButtonsOrderEquals([
      'plugin_reference_test_access_block' => self::BLOCK_PLUGINS['plugin_reference_test_access_block'],
      'plugin_reference_test_block' => self::BLOCK_PLUGINS['plugin_reference_test_block'],
      'plugin_reference_test_caching_block' => self::BLOCK_PLUGINS['plugin_reference_test_caching_block'],
    ], sprintf('%s[plugin_id]', $this->fieldName));
  }

}
