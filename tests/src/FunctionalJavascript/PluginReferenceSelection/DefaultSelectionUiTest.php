<?php

namespace Drupal\Tests\pluginreference\FunctionalJavascript\PluginReferenceSelection;

use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\field\Entity\FieldConfig;

/**
 * Test the usage of the default selection in the UI.
 *
 * @group pluginreference
 */
class DefaultSelectionUiTest extends SelectionUiTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'file',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Test selection handler in the select widget.
   */
  public function testPluginReferenceSelectWidget() {
    $page = $this->getSession()->getPage();

    EntityFormDisplay::load(sprintf('node.%s.default', $this->nodeType->id()))
      ->setComponent($this->fieldName, [
        'type' => 'plugin_reference_select',
        'settings' => [
          'configuration_form' => 'hidden',
          'provider_grouping' => FALSE,
        ],
      ])->save();

    $this->drupalGet(sprintf('node/add/%s', $this->nodeType->id()));

    // By default the plugin options are sorted ASC by label.
    $plugin_id = $page->findField(sprintf('%s[0][plugin_id]', $this->fieldName));
    $block_plugins_sorted_by_label_asc = self::BLOCK_PLUGINS;
    asort($block_plugins_sorted_by_label_asc);
    $this->assertSelectOptionsOrderEquals($block_plugins_sorted_by_label_asc, $plugin_id);

    // Update the field config so the plugin options are now sorted DESC by
    // plugin ID.
    /** @var \Drupal\field\FieldConfigInterface $field_config */
    $field_config = FieldConfig::loadByName('node', $this->nodeType->id(), $this->fieldName);
    $field_config->setSetting('handler_settings', [
      'sort' => [
        'key' => 'id',
        'direction' => 'DESC',
      ],
    ]);
    $field_config->save();

    $this->drupalGet(sprintf('node/add/%s', $this->nodeType->id()));

    // Check that options are sorted DESC by plugin ID.
    $plugin_id = $page->findField(sprintf('%s[0][plugin_id]', $this->fieldName));
    $block_plugins_sorted_by_plugin_id_desc = self::BLOCK_PLUGINS;
    ksort($block_plugins_sorted_by_plugin_id_desc);
    $this->assertSelectOptionsOrderEquals($block_plugins_sorted_by_plugin_id_desc, $plugin_id);
  }

  /**
   * Test selection handler in the autocomplete widget.
   */
  public function testPluginReferenceAutocompleteWidget() {
    $page = $this->getSession()->getPage();
    $assert_session = $this->assertSession();

    EntityFormDisplay::load(sprintf('node.%s.default', $this->nodeType->id()))
      ->setComponent($this->fieldName, [
        'type' => 'plugin_reference_autocomplete',
        'settings' => ['configuration_form' => 'hidden'],
      ])->save();

    $this->drupalGet(sprintf('node/add/%s', $this->nodeType->id()));

    // By default the plugin options are sorted ASC by label.
    $this->assertAutocompleteOrderEquals([
      'Test access block',
      'Test block',
      'Test caching block',
    ], sprintf('%s[0][plugin_id]', $this->fieldName), 'test');

    // Update the field config so the plugin options are now sorted DESC by
    // plugin ID.
    /** @var \Drupal\field\FieldConfigInterface $field_config */
    $field_config = FieldConfig::loadByName('node', $this->nodeType->id(), $this->fieldName);
    $field_config->setSetting('handler_settings', [
      'sort' => [
        'key' => 'id',
        'direction' => 'DESC',
      ],
    ]);
    $field_config->save();

    $this->drupalGet(sprintf('node/add/%s', $this->nodeType->id()));

    // Check that options are sorted DESC by plugin ID.
    $this->assertAutocompleteOrderEquals([
      self::BLOCK_PLUGINS['plugin_reference_test_caching_block'],
      self::BLOCK_PLUGINS['plugin_reference_test_block'],
      self::BLOCK_PLUGINS['plugin_reference_test_access_block'],
    ], sprintf('%s[0][plugin_id]', $this->fieldName), 'test');

    // Check that the test access block is not visible since this user doesn't
    // have access to it.
    $this->drupalLogin($this->userWithoutAccess);
    $this->drupalGet(sprintf('node/add/%s', $this->nodeType->id()));
    // Check that options are sorted DESC by plugin ID.
    $this->assertAutocompleteOrderEquals([
      self::BLOCK_PLUGINS['plugin_reference_test_caching_block'],
      self::BLOCK_PLUGINS['plugin_reference_test_block'],
    ], sprintf('%s[0][plugin_id]', $this->fieldName), 'test');

    // Check that saving the field with a plugin the user doesn't have access
    // to doesn't work.
    $page->fillField('title[0][value]', $this->randomMachineName());
    $page->fillField(sprintf('%s[0][plugin_id]', $this->fieldName), 'Test access block (plugin_reference_test_access_block)');
    $page->pressButton('Save');

    $assert_session->pageTextContains('This plugin (block: plugin_reference_test_access_block) cannot be referenced.');
  }

  /**
   * Test selection handler in the options button widget.
   */
  public function testPluginReferenceOptionsButtonsWidget() {
    EntityFormDisplay::load(sprintf('node.%s.default', $this->nodeType->id()))
      ->setComponent($this->fieldName, [
        'type' => 'plugin_reference_options_buttons',
        'settings' => ['configuration_form' => 'hidden'],
      ])->save();

    $this->drupalGet(sprintf('node/add/%s', $this->nodeType->id()));

    // By default the plugin options are sorted ASC by label.
    $block_plugins_sorted_by_label_asc = self::BLOCK_PLUGINS;
    asort($block_plugins_sorted_by_label_asc);
    $this->assertOptionButtonsOrderEquals($block_plugins_sorted_by_label_asc, sprintf('%s[plugin_id]', $this->fieldName));

    // Update the field config so the plugin options are now sorted DESC by
    // plugin ID.
    /** @var \Drupal\field\FieldConfigInterface $field_config */
    $field_config = FieldConfig::loadByName('node', $this->nodeType->id(), $this->fieldName);
    $field_config->setSetting('handler_settings', [
      'sort' => [
        'key' => 'id',
        'direction' => 'DESC',
      ],
    ]);
    $field_config->save();

    $this->drupalGet(sprintf('node/add/%s', $this->nodeType->id()));

    // Check that options are sorted DESC by plugin ID.
    $block_plugins_sorted_by_plugin_id_desc = self::BLOCK_PLUGINS;
    ksort($block_plugins_sorted_by_plugin_id_desc);
    $this->assertOptionButtonsOrderEquals($block_plugins_sorted_by_plugin_id_desc, sprintf('%s[plugin_id]', $this->fieldName));
  }

}
