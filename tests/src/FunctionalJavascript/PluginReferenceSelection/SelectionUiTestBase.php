<?php

namespace Drupal\Tests\pluginreference\FunctionalJavascript\PluginReferenceSelection;

use Behat\Mink\Element\NodeElement;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\Tests\pluginreference\Traits\PluginReferenceTrait;

/**
 * Base class for plugin reference selection tests.
 */
abstract class SelectionUiTestBase extends WebDriverTestBase {

  use PluginReferenceTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The plugin reference field name.
   *
   * @var string
   */
  protected $fieldName = 'field_pluginreference';

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['node', 'field_ui', 'pluginreference_test'];

  /**
   * The node type.
   *
   * @var \Drupal\node\Entity\NodeType
   */
  protected $nodeType;

  /**
   * The plugin type we want to reference.
   *
   * @var string
   */
  protected $targetType = 'block';

  /**
   * User without access to the test access block plugin.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $userWithoutAccess;

  /**
   * An array of all possible block plugins.
   */
  protected const BLOCK_PLUGINS = [
    'system_menu_block:admin' => 'Administration',
    'system_breadcrumb_block' => 'Breadcrumbs',
    'broken' => 'Broken/Missing',
    'system_menu_block:footer' => 'Footer',
    'system_menu_block:main' => 'Main navigation',
    'system_main_block' => 'Main page content',
    'system_messages_block' => 'Messages',
    'page_title_block' => 'Page title',
    'system_powered_by_block' => 'Powered by Drupal',
    'local_actions_block' => 'Primary admin actions',
    'system_branding_block' => 'Site branding',
    'node_syndicate_block' => 'Syndicate',
    'local_tasks_block' => 'Tabs',
    'plugin_reference_test_access_block' => 'Test access block',
    'plugin_reference_test_block' => 'Test block',
    'plugin_reference_test_caching_block' => 'Test caching block',
    'system_menu_block:tools' => 'Tools',
    'system_menu_block:account' => 'User account menu',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->drupalLogin($this->rootUser);
    $this->nodeType = $this->drupalCreateContentType();
    $this->userWithoutAccess = $this->createUser([
      sprintf('create %s content', $this->nodeType->id()),
      'pluginreference autocomplete view results',
    ]);
    $this->createPluginReferenceField('node', $this->nodeType->id(), $this->fieldName, $this->randomMachineName(), $this->targetType);
  }

  /**
   * Assert that the options of the given select field have the same order.
   *
   * @param array $expected
   *   The expected order of the select options.
   * @param \Behat\Mink\Element\NodeElement $select
   *   The select field.
   */
  protected function assertSelectOptionsOrderEquals(array $expected, NodeElement $select): void {
    $plugin_options = [];

    foreach ($select->findAll('css', 'option') as $option) {
      $plugin_options[$option->getValue()] = $option->getText();
    }

    // Remove the '- none -' option, when available.
    if (isset($plugin_options[''])) {
      unset($plugin_options['']);
    }

    $this->assertEquals($expected, $plugin_options);
  }

  /**
   * Assert that the autocomplete options have the same order.
   *
   * @param array $expected
   *   The expected order of the autocomplete.
   * @param string $field_name
   *   The name of the autocomplete field.
   * @param string $keyword
   *   The keyword to search.
   *
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   */
  protected function assertAutocompleteOrderEquals(array $expected, string $field_name, string $keyword) {
    $plugin_id_field = $this->assertSession()->fieldExists($field_name);
    $plugin_id_field->setValue($keyword);
    $plugin_id_field->keyDown(' ');
    $autocomplete = $this->assertSession()->waitOnAutocomplete()->getParent();
    $autocomplete_options = $autocomplete->findAll('css', 'li');

    $plugin_options = [];
    foreach ($autocomplete_options as $autocomplete_option) {
      $plugin_options[] = $autocomplete_option->find('css', 'a')->getText();
    }

    $this->assertEquals($expected, $plugin_options);
  }

  /**
   * Assert that the given radios or checkboxes field have the same order.
   *
   * @param array $expected
   *   The expected order of the radios or checkboxes buttons.
   * @param string $field_name
   *   The field to check.
   */
  protected function assertOptionButtonsOrderEquals(array $expected, string $field_name) {
    $option_buttons = $this->getSession()->getPage()->findAll('css', sprintf('input[name="%s"]', $field_name));
    $plugin_options = [];

    foreach ($option_buttons as $option_button) {
      $plugin_options[$option_button->getAttribute('value')] = $option_button->getParent()->find('css', 'label')->getText();
    }

    // Remove the 'N/A' option, when available.
    if (isset($plugin_options['_none'])) {
      unset($plugin_options['_none']);
    }

    $this->assertEquals($expected, $plugin_options);
  }

}
