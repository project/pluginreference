<?php

namespace Drupal\Tests\pluginreference\FunctionalJavascript;

use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Url;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\Tests\pluginreference\Traits\PluginReferenceTrait;

/**
 * Test the pluginreference widgets with configuration enabled.
 *
 * @group pluginreference
 */
class PluginReferenceFieldWidgetConfigurationTest extends WebDriverTestBase {

  use PluginReferenceTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The plugin reference field name.
   *
   * @var string
   */
  protected $fieldName = 'field_pluginreference';

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['node', 'field_ui', 'pluginreference_test'];

  /**
   * The node type.
   *
   * @var \Drupal\node\Entity\NodeType
   */
  protected $nodeType;

  /**
   * The plugin type we want to reference.
   *
   * @var string
   */
  protected $targetType = 'block';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->drupalLogin($this->rootUser);
    $this->nodeType = $this->drupalCreateContentType();
    $this->createPluginReferenceField('node', $this->nodeType->id(), $this->fieldName, $this->randomMachineName(), $this->targetType);
  }

  /**
   * Test plugin reference select widget with configuration.
   */
  public function testPluginReferenceSelectWidgetWithConfiguration() {
    $assert_session = $this->assertSession();
    $page = $this->getSession()->getPage();
    EntityFormDisplay::load(sprintf('node.%s.default', $this->nodeType->id()))
      ->setComponent($this->fieldName, [
        'type' => 'plugin_reference_select',
        'settings' => [
          'provider_grouping' => FALSE,
          'configuration_form' => 'full',
        ],
      ])->save();

    $this->drupalGet(Url::fromRoute('node.add', ['node_type' => $this->nodeType->id()]));

    $page->findField('title[0][value]')->setValue('Test pluginreference');

    $plugin_id_field = $assert_session->fieldExists(sprintf('%s[0][plugin_id]', $this->fieldName));
    $plugin_id_field->selectOption('plugin_reference_test_block');

    $test_value_field = $assert_session->waitForField(sprintf('%s[0][configuration][test_value]', $this->fieldName));
    $test_value_field->setValue('fail');
    $page->pressButton('Save');
    $assert_session->responseContains('Value should not be fail.');

    $test_value_field->setValue('test value');
    $page->pressButton('Save');

    $node = $this->drupalGetNodeByTitle('Test pluginreference');
    $this->assertEquals('plugin_reference_test_block', $node->get($this->fieldName)->plugin_id);
    $this->assertEquals('test value', $node->get($this->fieldName)->configuration['test_value']);

    // Test select widget with cardinality unlimited.
    $this->drupalGet(Url::fromRoute('entity.field_config.node_field_edit_form', [
      'node_type' => $this->nodeType->id(),
      'field_config' => sprintf('%s.%s.%s', 'node', $this->nodeType->id(), $this->fieldName),
    ]));
    $cardinality = $assert_session->fieldExists('field_storage[subform][cardinality]');
    $cardinality->setValue(-1);
    $assert_session->assertWaitOnAjaxRequest();
    $page->pressButton('Save settings');
    drupal_flush_all_caches();

    $this->drupalGet(Url::fromRoute('node.add', ['node_type' => $this->nodeType->id()]));
    $page->findField('title[0][value]')->setValue('Test pluginreference 2');

    $plugin_id_field = $assert_session->fieldExists(sprintf('%s[0][plugin_id]', $this->fieldName));
    $plugin_id_field->selectOption('plugin_reference_test_block');

    $test_value_field = $assert_session->waitForField(sprintf('%s[0][configuration][test_value]', $this->fieldName));
    $test_value_field->setValue('test');
    $page->pressButton('Add another item');

    // Check if system_branding_block works, because it depends on the
    // block_theme in the form_state object.
    $plugin_id_field = $assert_session->waitForField(sprintf('%s[1][plugin_id]', $this->fieldName));
    $plugin_id_field->selectOption('system_branding_block');

    $site_logo_field = $assert_session->waitForField(sprintf('%s[1][configuration][block_branding][use_site_logo]', $this->fieldName));
    $site_logo_field->uncheck();
    $site_slogan_field = $assert_session->fieldExists(sprintf('%s[1][configuration][block_branding][use_site_slogan]', $this->fieldName));
    $site_slogan_field->uncheck();
    $page->pressButton('Save');

    $node = $this->drupalGetNodeByTitle('Test pluginreference 2');

    // Check that the values are correctly saved.
    $this->assertEquals('plugin_reference_test_block', $node->get($this->fieldName)->get(0)->get('plugin_id')->getValue());
    $this->assertEquals('test', $node->get($this->fieldName)->get(0)->get('configuration')->getValue()['test_value']);
    $this->assertEquals('system_branding_block', $node->get($this->fieldName)->get(1)->get('plugin_id')->getValue());
    $this->assertFalse((bool) $node->get($this->fieldName)->get(1)->get('configuration')->getValue()['use_site_logo']);
    $this->assertFalse((bool) $node->get($this->fieldName)->get(1)->get('configuration')->getValue()['use_site_slogan']);
    $this->assertTrue((bool) $node->get($this->fieldName)->get(1)->get('configuration')->getValue()['use_site_name']);

    // Check that the configuration is correctly shown on the node edit form.
    $this->drupalGet(Url::fromRoute('entity.node.edit_form', ['node' => $node->id()]));
    $assert_session->fieldValueEquals(sprintf('%s[0][plugin_id]', $this->fieldName), 'plugin_reference_test_block');
    $assert_session->fieldValueEquals(sprintf('%s[0][configuration][test_value]', $this->fieldName), 'test');
    $assert_session->fieldValueEquals(sprintf('%s[1][plugin_id]', $this->fieldName), 'system_branding_block');
    $assert_session->checkboxNotChecked(sprintf('%s[1][configuration][block_branding][use_site_logo]', $this->fieldName));
    $assert_session->checkboxNotChecked(sprintf('%s[1][configuration][block_branding][use_site_slogan]', $this->fieldName));
    $assert_session->checkboxChecked(sprintf('%s[1][configuration][block_branding][use_site_name]', $this->fieldName));
  }

  /**
   * Test plugin reference autocomplete with configuration.
   */
  public function testPluginReferenceAutocompleteWidgetWithConfiguration() {
    $assert_session = $this->assertSession();
    $page = $this->getSession()->getPage();
    EntityFormDisplay::load(sprintf('node.%s.default', $this->nodeType->id()))
      ->setComponent($this->fieldName, [
        'type' => 'plugin_reference_autocomplete',
        'settings' => ['configuration_form' => 'full'],
      ])->save();

    $this->drupalGet(Url::fromRoute('node.add', ['node_type' => $this->nodeType->id()]));

    $page->findField('title[0][value]')->setValue('Test pluginreference');

    $plugin_id_field = $assert_session->fieldExists(sprintf('%s[0][plugin_id]', $this->fieldName));
    $plugin_id_field->setValue('test block');
    $plugin_id_field->keyDown(' ');
    $assert_session->waitOnAutocomplete()->click();

    $test_value_field = $assert_session->waitForField(sprintf('%s[0][configuration][test_value]', $this->fieldName));
    $test_value_field->setValue('fail');
    $page->pressButton('Save');
    $assert_session->responseContains('Value should not be fail.');

    $test_value_field->setValue('test value');
    $page->pressButton('Save');

    $node = $this->drupalGetNodeByTitle('Test pluginreference');
    $this->assertEquals('plugin_reference_test_block', $node->get($this->fieldName)->plugin_id);
    $this->assertEquals('test value', $node->get($this->fieldName)->configuration['test_value']);

    // Test autocomplete widget with cardinality unlimited.
    $this->drupalGet(Url::fromRoute('entity.field_config.node_field_edit_form', [
      'node_type' => $this->nodeType->id(),
      'field_config' => sprintf('%s.%s.%s', 'node', $this->nodeType->id(), $this->fieldName),
    ]));
    $cardinality = $assert_session->fieldExists('field_storage[subform][cardinality]');
    $cardinality->setValue(-1);
    $assert_session->assertWaitOnAjaxRequest();
    $page->pressButton('Save settings');
    drupal_flush_all_caches();

    $this->drupalGet(Url::fromRoute('node.add', ['node_type' => $this->nodeType->id()]));
    $page->findField('title[0][value]')->setValue('Test pluginreference 2');

    $plugin_id_field = $assert_session->fieldExists(sprintf('%s[0][plugin_id]', $this->fieldName));
    $plugin_id_field->setValue('test block');
    $plugin_id_field->keyDown(' ');
    $assert_session->waitOnAutocomplete()->click();

    $test_value_field = $assert_session->waitForField(sprintf('%s[0][configuration][test_value]', $this->fieldName));
    $test_value_field->setValue('test');
    $page->pressButton('Add another item');

    $plugin_id_field = $assert_session->waitForField(sprintf('%s[1][plugin_id]', $this->fieldName));
    $plugin_id_field->setValue('tabs');
    $plugin_id_field->keyDown(' ');
    $assert_session->waitOnAutocomplete()->click();

    $primary_field = $assert_session->waitForField(sprintf('%s[1][configuration][levels][primary]', $this->fieldName));
    $page->pressButton('Shown tabs');
    $primary_field->check();

    $secondary_field = $assert_session->fieldExists(sprintf('%s[1][configuration][levels][secondary]', $this->fieldName));
    $secondary_field->uncheck();

    $page->pressButton('Save');

    $node = $this->drupalGetNodeByTitle('Test pluginreference 2');

    // Check that the values are correctly saved.
    $this->assertEquals('plugin_reference_test_block', $node->get($this->fieldName)->get(0)->get('plugin_id')->getValue());
    $this->assertEquals('test', $node->get($this->fieldName)->get(0)->get('configuration')->getValue()['test_value']);
    $this->assertEquals('local_tasks_block', $node->get($this->fieldName)->get(1)->get('plugin_id')->getValue());
    $this->assertTrue((bool) $node->get($this->fieldName)->get(1)->get('configuration')->getValue()['primary']);
    $this->assertFalse((bool) $node->get($this->fieldName)->get(1)->get('configuration')->getValue()['secondary']);

    // Check that the configuration is correctly shown on the node edit form.
    $this->drupalGet(Url::fromRoute('entity.node.edit_form', ['node' => $node->id()]));
    $assert_session->fieldValueEquals(sprintf('%s[0][plugin_id]', $this->fieldName), 'Test block (plugin_reference_test_block)');
    $assert_session->fieldValueEquals(sprintf('%s[0][configuration][test_value]', $this->fieldName), 'test');
    $assert_session->fieldValueEquals(sprintf('%s[1][plugin_id]', $this->fieldName), 'Tabs (local_tasks_block)');
    $assert_session->checkboxChecked(sprintf('%s[1][configuration][levels][primary]', $this->fieldName));
    $assert_session->checkboxNotChecked(sprintf('%s[1][configuration][levels][secondary]', $this->fieldName));
  }

  /**
   * Test plugin reference options buttons with configuration.
   */
  public function testPluginReferenceOptionsButtonsWithConfiguration() {
    $assert_session = $this->assertSession();
    $page = $this->getSession()->getPage();

    EntityFormDisplay::load(sprintf('node.%s.default', $this->nodeType->id()))
      ->setComponent($this->fieldName, [
        'type' => 'plugin_reference_options_buttons',
        'settings' => ['configuration_form' => 'full'],
      ])->save();

    $this->drupalGet(Url::fromRoute('node.add', ['node_type' => $this->nodeType->id()]));

    $page->findField('title[0][value]')->setValue('Test pluginreference');

    $plugin_id_field = $assert_session->fieldExists(sprintf('%s[plugin_id]', $this->fieldName));
    $plugin_id_field->setValue('plugin_reference_test_block');

    $test_value_field = $assert_session->waitForField(sprintf('%s[configuration][plugin_reference_test_block][test_value]', $this->fieldName));
    $test_value_field->setValue('fail');
    $page->pressButton('Save');
    $assert_session->responseContains('Value should not be fail.');

    $test_value_field->setValue('test value');
    $page->pressButton('Save');

    $node = $this->drupalGetNodeByTitle('Test pluginreference');
    $this->assertEquals('plugin_reference_test_block', $node->get($this->fieldName)->plugin_id);
    $this->assertEquals('test value', $node->get($this->fieldName)->configuration['test_value']);

    // Test options button widget with cardinality unlimited.
    $this->drupalGet(Url::fromRoute('entity.field_config.node_field_edit_form', [
      'node_type' => $this->nodeType->id(),
      'field_config' => sprintf('%s.%s.%s', 'node', $this->nodeType->id(), $this->fieldName),
    ]));
    $cardinality = $assert_session->fieldExists('field_storage[subform][cardinality]');
    $cardinality->setValue(-1);
    $assert_session->assertWaitOnAjaxRequest();
    $page->pressButton('Save settings');
    drupal_flush_all_caches();

    $this->drupalGet(Url::fromRoute('node.add', ['node_type' => $this->nodeType->id()]));
    $page->findField('title[0][value]')->setValue('Test pluginreference 2');

    // The plugin_id_field are now checkboxes.
    $page->checkField(sprintf('%s[plugin_id][plugin_reference_test_block]', $this->fieldName));

    $test_value_field = $assert_session->waitForField(sprintf('%s[configuration][plugin_reference_test_block][test_value]', $this->fieldName));
    $test_value_field->setValue('test');

    $page->checkField(sprintf('%s[plugin_id][system_branding_block]', $this->fieldName));

    $site_logo_field = $assert_session->waitForField(sprintf('%s[configuration][system_branding_block][block_branding][use_site_logo]', $this->fieldName));
    $site_logo_field->uncheck();
    $site_slogan_field = $assert_session->fieldExists(sprintf('%s[configuration][system_branding_block][block_branding][use_site_slogan]', $this->fieldName));
    $site_slogan_field->uncheck();
    $page->pressButton('Save');

    $node = $this->drupalGetNodeByTitle('Test pluginreference 2');

    // Check that the values are correctly saved.
    $this->assertEquals('system_branding_block', $node->get($this->fieldName)->get(0)->get('plugin_id')->getValue());
    $this->assertFalse((bool) $node->get($this->fieldName)->get(0)->get('configuration')->getValue()['use_site_logo']);
    $this->assertFalse((bool) $node->get($this->fieldName)->get(0)->get('configuration')->getValue()['use_site_slogan']);
    $this->assertTrue((bool) $node->get($this->fieldName)->get(0)->get('configuration')->getValue()['use_site_name']);
    $this->assertEquals('plugin_reference_test_block', $node->get($this->fieldName)->get(1)->get('plugin_id')->getValue());
    $this->assertEquals('test', $node->get($this->fieldName)->get(1)->get('configuration')->getValue()['test_value']);

    // Check that the configuration is correctly shown on the node edit form.
    $this->drupalGet(Url::fromRoute('entity.node.edit_form', ['node' => $node->id()]));
    $assert_session->checkboxChecked(sprintf('%s[plugin_id][plugin_reference_test_block]', $this->fieldName));
    $assert_session->fieldValueEquals(sprintf('%s[configuration][plugin_reference_test_block][test_value]', $this->fieldName), 'test');
    $assert_session->checkboxChecked(sprintf('%s[plugin_id][system_branding_block]', $this->fieldName));
    $assert_session->checkboxNotChecked(sprintf('%s[configuration][system_branding_block][block_branding][use_site_logo]', $this->fieldName));
    $assert_session->checkboxNotChecked(sprintf('%s[configuration][system_branding_block][block_branding][use_site_slogan]', $this->fieldName));
    $assert_session->checkboxChecked(sprintf('%s[configuration][system_branding_block][block_branding][use_site_name]', $this->fieldName));
  }

}
