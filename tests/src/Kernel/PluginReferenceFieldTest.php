<?php

namespace Drupal\Tests\pluginreference\Kernel;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\pluginreference_test\Plugin\Block\PluginreferenceTestBlock;
use Drupal\pluginreference_test\Plugin\Block\PluginreferenceTestCachingBlock;
use Drupal\system\Plugin\Block\SystemBrandingBlock;
use Drupal\Tests\pluginreference\Traits\PluginReferenceTrait;

/**
 * Test the pluginreference field functionality.
 *
 * @group pluginreference
 */
class PluginReferenceFieldTest extends EntityKernelTestBase {

  use PluginReferenceTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'pluginreference',
    'pluginreference_test',
    'comment',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Add a plugin reference field.
    $this->createPluginReferenceField(
      'entity_test',
      'entity_test',
      'field_test',
      'Test field',
      'block',
      'default',
      [],
      FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED
    );
  }

  /**
   * Test ::referencedPlugins().
   */
  public function testReferencedPlugins() {
    $entity = EntityTest::create([
      'field_test' => [
        ['plugin_id' => 'system_branding_block'],
        ['plugin_id' => 'plugin_reference_test_block'],
        ['plugin_id' => 'plugin_reference_test_caching_block'],
      ],
    ]);
    $entity->save();

    // Make sure that referencedPlugins returns all saved plugins.
    $referenced_plugins = $entity->field_test->referencedPlugins();
    $this->assertCount(3, $referenced_plugins);
    $this->assertInstanceOf(SystemBrandingBlock::class, $referenced_plugins[0]);
    $this->assertInstanceOf(PluginreferenceTestBlock::class, $referenced_plugins[1]);
    $this->assertInstanceOf(PluginreferenceTestCachingBlock::class, $referenced_plugins[2]);

    // Only return valid plugins.
    $this->uninstallModule('pluginreference_test');
    $entity = $this->reloadEntity($entity);
    $referenced_plugins = $entity->field_test->referencedPlugins();
    $this->assertCount(1, $referenced_plugins);
    $this->assertInstanceOf(SystemBrandingBlock::class, $referenced_plugins[0]);
  }

  /**
   * Test ::referencedPlugin().
   */
  public function testReferencedPlugin() {
    $entity = EntityTest::create([
      'field_test' => [
        ['plugin_id' => 'system_branding_block'],
        ['plugin_id' => 'plugin_reference_test_block'],
        ['plugin_id' => 'plugin_reference_test_caching_block'],
      ],
    ]);
    $entity->save();

    // Make sure referencedPlugin returns the saved plugin.
    $this->assertInstanceOf(SystemBrandingBlock::class, $entity->field_test->get(0)->referencedPlugin());
    $this->assertInstanceOf(PluginreferenceTestBlock::class, $entity->field_test->get(1)->referencedPlugin());
    $this->assertInstanceOf(PluginreferenceTestCachingBlock::class, $entity->field_test->get(2)->referencedPlugin());

    // Only return valid plugins.
    $this->uninstallModule('pluginreference_test');
    $entity = $this->reloadEntity($entity);
    $this->assertInstanceOf(SystemBrandingBlock::class, $entity->field_test->get(0)->referencedPlugin());
    $this->assertEmpty($entity->field_test->get(1)->referencedPlugin());
    $this->assertEmpty($entity->field_test->get(2)->referencedPlugin());
  }

  /**
   * Test ->target_id.
   */
  public function testTargetId() {
    $entity = EntityTest::create([
      'field_test' => [
        ['plugin_id' => 'system_branding_block'],
        ['plugin_id' => 'plugin_reference_test_block'],
        ['plugin_id' => 'plugin_reference_test_caching_block'],
      ],
    ]);
    $entity->save();

    $this->assertEquals('system_branding_block', $entity->field_test->get(0)->plugin_id);
    $this->assertEquals('plugin_reference_test_block', $entity->field_test->get(1)->plugin_id);
    $this->assertEquals('plugin_reference_test_caching_block', $entity->field_test->get(2)->plugin_id);
  }

  /**
   * Tests the dependencies plugin reference fields are created with.
   */
  public function testPluginReferenceFieldDependencies() {
    $field_name = 'widget_reference_field';
    $entity_type = 'entity_test';

    $field_storage = FieldStorageConfig::create([
      'field_name' => $field_name,
      'type' => 'plugin_reference',
      'entity_type' => $entity_type,
      'settings' => [
        'target_type' => 'field.widget',
      ],
    ]);
    $field_storage->save();
    // The system module provides the plugin manager for field widgets, so
    // this module should be added as a dependency.
    $this->assertEquals([
      'module' => [
        'entity_test',
        'pluginreference',
        'system',
      ],
    ], $field_storage->getDependencies());

    $field = FieldConfig::create([
      'field_name' => $field_name,
      'entity_type' => $entity_type,
      'bundle' => 'entity_test',
      'label' => $field_name,
      'settings' => [
        'handler' => 'field_widget_advanced',
      ],
      'default_value' => [
        [
          'plugin_id' => 'comment_default',
          'configuration' => [],
        ],
      ],
    ]);
    $field->save();

    // The field_widget_advanced handler is provided by pluginreference_test and
    // comment_default plugin by comment, so both modules should be added as a
    // dependency.
    $this->assertEquals([
      'config' => ['field.storage.entity_test.widget_reference_field'],
      'module' => [
        'comment',
        'entity_test',
        'pluginreference',
        'pluginreference_test',
      ],
    ], $field->getDependencies());
  }

}
