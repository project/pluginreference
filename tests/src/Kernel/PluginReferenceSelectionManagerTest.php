<?php

namespace Drupal\Tests\pluginreference\Kernel;

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\KernelTests\KernelTestBase;
use Drupal\pluginreference\Plugin\PluginReferenceSelection\DefaultSelection;
use Drupal\pluginreference_test\Plugin\PluginReferenceSelection\FieldWidgetTestSelection;

/**
 * Tests the PluginReferenceSelectionManager class.
 *
 * @group pluginreference
 *
 * @coversDefaultClass \Drupal\pluginreference\PluginReferenceSelectionManager
 */
class PluginReferenceSelectionManagerTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'pluginreference',
    'pluginreference_test',
    'entity_test',
    'field',
    'user',
  ];

  /**
   * The plugin reference selection manager.
   *
   * @var \Drupal\pluginreference\PluginReferenceSelectionManagerInterface
   */
  protected $pluginReferenceSelectionManager;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('entity_test');
    $this->installConfig(['field']);

    $this->pluginReferenceSelectionManager = $this->container->get('plugin.manager.plugin_reference_selection');
  }

  /**
   * Tests the ::getInstance method.
   *
   * @dataProvider getInstanceDataProvider
   *
   * @covers ::getInstance
   */
  public function testGetInstance(string $expected, array $options) {
    if (!isset($options['target_type'])) {
      $this->expectException(\InvalidArgumentException::class);
      $this->pluginReferenceSelectionManager->getInstance($options);
    }
    else {
      $this->assertInstanceOf($expected, $this->pluginReferenceSelectionManager->getInstance($options));
    }
  }

  /**
   * Data provider for testGetInstance().
   *
   * @see testGetInstance()
   */
  public static function getInstanceDataProvider() {
    return [
      [
        DefaultSelection::class,
        [
          'target_type' => 'plugin_reference_selection',
        ],
      ],
      [
        DefaultSelection::class,
        [
          'target_type' => 'block',
        ],
      ],
      [
        FieldWidgetTestSelection::class,
        [
          'target_type' => 'field.widget',
        ],
      ],
      [
        DefaultSelection::class,
        [
          'target_type' => 'field.widget',
          'handler' => 'default:field.widget',
        ],
      ],
      [
        FieldWidgetTestSelection::class,
        [
          'target_type' => 'field.widget',
          'handler' => 'default',
        ],
      ],
      [
        '',
        [],
      ],
    ];
  }

  /**
   * Tests the ::getPluginId method.
   *
   * @dataProvider getPluginIdDataProvider
   *
   * @covers ::getPluginId
   */
  public function testGetPluginId(string $expected, string $target_type, string $base_plugin_id) {
    $this->assertEquals($expected, $this->pluginReferenceSelectionManager->getPluginId($target_type, $base_plugin_id));
  }

  /**
   * Data provider for testGetPluginId().
   *
   * @see testGetPluginId()
   */
  public static function getPluginIdDataProvider() {
    return [
      [
        'default:plugin_reference_selection',
        'plugin_reference_selection',
        'default',
      ],
      [
        'default:block',
        'block',
        'default',
      ],
      [
        'default:field_widget_test',
        'field.widget',
        'default',
      ],
    ];
  }

  /**
   * Tests the ::getSelectionGroups method.
   *
   * @dataProvider getSelectionGroupsDataProvider
   *
   * @covers ::getSelectionGroups
   */
  public function testGetSelectionGroups(array $expected, string $target_type) {
    $selection_groups = $this->pluginReferenceSelectionManager->getSelectionGroups($target_type);

    foreach ($expected as $base_plugin_id => $expected_plugin_ids) {
      $this->assertArrayHasKey($base_plugin_id, $selection_groups);
      $this->assertEquals($expected_plugin_ids, array_keys($selection_groups[$base_plugin_id]));
    }

    $this->assertArrayNotHasKey('broken', $selection_groups);
  }

  /**
   * Data provider for testGetSelectionGroups().
   *
   * @see testGetSelectionGroups()
   */
  public static function getSelectionGroupsDataProvider() {
    return [
      [
        [
          'default' => [
            'default:plugin_reference_selection',
          ],
        ],
        'plugin_reference_selection',
      ],
      [
        [
          'default' => [
            'default:field.widget',
            'default:field_widget_test',
          ],
        ],
        'field.widget',
      ],
      [
        [
          'default' => [
            'default:block',
          ],
        ],
        'block',
      ],
    ];
  }

  /**
   * Tests the ::getSelectionHandler method.
   *
   * @dataProvider getSelectionHandlerDataProvider
   *
   * @covers ::getSelectionHandler
   */
  public function testGetSelectionHandler(string $expected, string $target_type, string $handler, array $handler_settings) {
    $field_name = mb_strtolower($this->randomMachineName());
    $field_storage = FieldStorageConfig::create([
      'field_name' => $field_name,
      'entity_type' => 'entity_test',
      'type' => 'plugin_reference',
      'settings' => ['target_type' => $target_type],
    ]);
    $field_storage->save();
    $field = FieldConfig::create([
      'field_storage' => $field_storage,
      'bundle' => 'entity_test',
      'settings' => [
        'handler' => $handler,
        'handler_settings' => $handler_settings,
      ],
    ]);
    $field->save();

    $this->assertInstanceOf($expected, $this->pluginReferenceSelectionManager->getSelectionHandler($field));
  }

  /**
   * Data provider for testGetSelectionHandler().
   *
   * @see testGetSelectionHandler()
   */
  public static function getSelectionHandlerDataProvider() {
    return [
      [
        DefaultSelection::class,
        'plugin_reference_selection',
        'default:plugin_reference_selection',
        [],
      ],
      [
        DefaultSelection::class,
        'block',
        'default:block',
        [],
      ],
      [
        FieldWidgetTestSelection::class,
        'field.widget',
        'default:field_widget_test',
        [],
      ],
      [
        DefaultSelection::class,
        'field.widget',
        'default:field.widget',
        [],
      ],
      [
        FieldWidgetTestSelection::class,
        'field.widget',
        'default',
        [],
      ],
    ];
  }

  /**
   * Tests the ::getFallbackPluginId method.
   *
   * @covers ::getFallbackPluginId
   */
  public function testGetFallbackPluginId() {
    $this->assertEquals('broken', $this->pluginReferenceSelectionManager->getFallbackPluginId('', []));
  }

}
