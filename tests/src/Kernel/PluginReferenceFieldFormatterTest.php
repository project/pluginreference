<?php

namespace Drupal\Tests\pluginreference\Kernel;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\pluginreference\Traits\PluginReferenceTrait;
use Drupal\user\RoleInterface;

/**
 * Test the pluginreference formatter functionality.
 *
 * @group pluginreference
 */
class PluginReferenceFieldFormatterTest extends KernelTestBase {

  use PluginReferenceTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'field',
    'image',
    'text',
    'entity_test',
    'user',
    'system',
    'pluginreference',
    'pluginreference_test',
  ];

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity display repository.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['field', 'user']);
    $this->installEntitySchema('entity_test');

    $this->entityTypeManager = $this->container->get('entity_type.manager');
    $this->entityDisplayRepository = $this->container->get('entity_display.repository');
  }

  /**
   * Test pluginreference ID formatter.
   *
   * @dataProvider pluginReferenceIdFormatterDataProvider
   */
  public function testPluginReferenceIdFormatter(string $entity_type, string $bundle, string $field_name, string $target_type, string $plugin_id, array $configuration, string $expected_output, array $expected_cacheability_metadata) {
    $this->createPluginReferenceField($entity_type, $bundle, $field_name, $this->randomMachineName(), $target_type);

    $entity = $this->entityTypeManager->getStorage($entity_type)->create([]);
    $entity->set($field_name, [
      'plugin_id' => $plugin_id,
      'configuration' => $configuration,
    ]);

    $build = $this->buildEntityField($entity, $field_name, 'plugin_reference_id');
    $this->render($build);
    $this->assertText($expected_output);
    $this->assertEquals(CacheableMetadata::createFromRenderArray(['#cache' => $expected_cacheability_metadata]), CacheableMetadata::createFromRenderArray($build[0]));
  }

  /**
   * Data provider for testPluginReferenceIdFormatter().
   *
   * @see testPluginReferenceIdFormatter()
   */
  public function pluginReferenceIdFormatterDataProvider() {
    return [
      [
        'entity_test',
        'entity_test',
        mb_strtolower($this->randomMachineName()),
        'image.effect',
        'image_desaturate',
        [],
        'image_desaturate',
        [
          'max-age' => -1,
        ],
      ],
      [
        'entity_test',
        'entity_test',
        mb_strtolower($this->randomMachineName()),
        'image.effect',
        'image_scale_and_crop',
        [],
        'image_scale_and_crop',
        [
          'max-age' => -1,
        ],
      ],
      [
        'entity_test',
        'entity_test',
        mb_strtolower($this->randomMachineName()),
        'block',
        'system_branding_block',
        [],
        'system_branding_block',
        [
          'tags' => ['config:system.site'],
        ],
      ],
      [
        'entity_test',
        'entity_test',
        mb_strtolower($this->randomMachineName()),
        'block',
        'plugin_reference_test_caching_block',
        [],
        'plugin_reference_test_caching_block',
        [
          'tags' => ['node_list'],
          'contexts' => ['url.query_args'],
          'max-age' => 3600,
        ],
      ],
      [
        'entity_test',
        'entity_test',
        mb_strtolower($this->randomMachineName()),
        'field.widget',
        'text_textfield',
        [],
        'text_textfield',
        [
          'max-age' => -1,
        ],
      ],
    ];
  }

  /**
   * Test pluginreference label formatter.
   *
   * @dataProvider pluginReferenceLabelFormatterDataProvider
   */
  public function testPluginReferenceLabelFormatter(string $entity_type, string $bundle, string $field_name, string $target_type, string $plugin_id, array $configuration, string $expected_output, array $expected_cacheability_metadata) {
    $this->createPluginReferenceField($entity_type, $bundle, $field_name, $this->randomMachineName(), $target_type);

    $entity = $this->entityTypeManager->getStorage($entity_type)->create([]);
    $entity->set($field_name, [
      'plugin_id' => $plugin_id,
      'configuration' => $configuration,
    ]);

    $build = $this->buildEntityField($entity, $field_name, 'plugin_reference_label');
    $this->render($build);
    $this->assertText($expected_output);
    $this->assertEquals(CacheableMetadata::createFromRenderArray(['#cache' => $expected_cacheability_metadata]), CacheableMetadata::createFromRenderArray($build[0]));
  }

  /**
   * Data provider for testPluginReferenceLabelFormatter().
   *
   * @see testPluginReferenceLabelFormatter()
   */
  public function pluginReferenceLabelFormatterDataProvider() {
    return [
      [
        'entity_test',
        'entity_test',
        mb_strtolower($this->randomMachineName()),
        'image.effect',
        'image_desaturate',
        [],
        'Desaturate',
        [
          'max-age' => -1,
        ],
      ],
      [
        'entity_test',
        'entity_test',
        mb_strtolower($this->randomMachineName()),
        'image.effect',
        'image_scale_and_crop',
        [],
        'Scale and crop',
        [
          'max-age' => -1,
        ],
      ],
      // A block plugin with no label configured, falls back to the configured
      // label in the annotation.
      [
        'entity_test',
        'entity_test',
        mb_strtolower($this->randomMachineName()),
        'block',
        'system_branding_block',
        [],
        'Site branding',
        [
          'tags' => ['config:system.site'],
        ],
      ],
      // A block plugin that has a label configured, uses the configured label.
      [
        'entity_test',
        'entity_test',
        mb_strtolower($this->randomMachineName()),
        'block',
        'system_branding_block',
        ['label' => 'Branding Block label'],
        'Branding Block label',
        [
          'tags' => ['config:system.site'],
        ],
      ],
      [
        'entity_test',
        'entity_test',
        mb_strtolower($this->randomMachineName()),
        'block',
        'plugin_reference_test_caching_block',
        [],
        'Test caching block',
        [
          'tags' => ['node_list'],
          'contexts' => ['url.query_args'],
          'max-age' => 3600,
        ],
      ],
      [
        'entity_test',
        'entity_test',
        mb_strtolower($this->randomMachineName()),
        'field.widget',
        'text_textfield',
        [],
        'Text field',
        [
          'max-age' => -1,
        ],
      ],
    ];
  }

  /**
   * Test access of plugins in the field formatters.
   *
   * @dataProvider accessDataProvider
   */
  public function testAccess(string $formatter) {
    $user_role_storage = $this->entityTypeManager->getStorage('user_role');
    $anonymous_role = $user_role_storage->load(RoleInterface::ANONYMOUS_ID);
    $anonymous_role->grantPermission('view plugin reference test access block');
    $anonymous_role->save();

    $field_name = mb_strtolower($this->randomMachineName());
    $this->createPluginReferenceField('entity_test', 'entity_test', $field_name, $this->randomMachineName(), 'block');

    $entity = $this->entityTypeManager->getStorage('entity_test')->create([]);
    $entity->set($field_name, [
      'plugin_id' => 'plugin_reference_test_access_block',
    ]);

    $build = $this->buildEntityField($entity, $field_name, $formatter);

    // The user has access, check if the plugin is present in the renderable
    // array.
    $this->assertArrayHasKey(0, $build);
    $expected_cacheability_metadata = [
      'contexts' => ['user.permissions'],
    ];
    $this->assertEquals(CacheableMetadata::createFromRenderArray(['#cache' => $expected_cacheability_metadata]), CacheableMetadata::createFromRenderArray($build));

    // Revoke permission so the user no longer has access to the plugin.
    $anonymous_role->revokePermission('view plugin reference test access block');
    $anonymous_role->save();

    // The user has no access to the plugin. Check if the plugin is removed from
    // the renderable array.
    $build = $this->buildEntityField($entity, $field_name, 'plugin_reference_label');
    $this->assertArrayNotHasKey(0, $build);
    $this->assertEquals(CacheableMetadata::createFromRenderArray(['#cache' => $expected_cacheability_metadata]), CacheableMetadata::createFromRenderArray($build));
  }

  /**
   * Data provider for testAccess().
   *
   * @see testAccess()
   */
  public static function accessDataProvider() {
    return [
      [
        'plugin_reference_id',
      ],
      [
        'plugin_reference_label',
      ],
    ];
  }

  /**
   * Helper method that builds a renderable array for a given entity field.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   The entity.
   * @param string $field_name
   *   The field name.
   * @param string $formatter_type
   *   The formatter type.
   * @param array $formatter_settings
   *   The formatter settings.
   *
   * @return array
   *   The field renderable array.
   */
  protected function buildEntityField(FieldableEntityInterface $entity, string $field_name, string $formatter_type, array $formatter_settings = []): array {
    $display = $this->entityDisplayRepository->getViewDisplay($entity->getEntityTypeId(), $entity->bundle());

    $display->setComponent($field_name, [
      'type' => $formatter_type,
      'settings' => $formatter_settings,
    ]);
    $display->save();

    $build = $display->build($entity);
    return $build[$field_name];
  }

}
