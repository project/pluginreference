<?php

namespace Drupal\Tests\pluginreference\Kernel\PluginReferenceSelection;

use Drupal\KernelTests\KernelTestBase;
use Drupal\pluginreference\Plugin\PluginReferenceSelection\BlockFilteredSelection;
use Drupal\user\RoleInterface;

/**
 * Tests the Plugin reference FilteredSelection plugin.
 *
 * @group pluginreference
 *
 * @coversDefaultClass \Drupal\pluginreference\Plugin\PluginReferenceSelection\FilteredSelection
 */
class FilteredSelectionTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'pluginreference',
    'pluginreference_test',
    'block',
    'entity_test',
    'field',
    'user',
    'system',
  ];

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The plugin reference selection manager.
   *
   * @var \Drupal\pluginreference\PluginReferenceSelectionManagerInterface
   */
  protected $pluginReferenceSelectionManager;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('entity_test');
    $this->installConfig(['field', 'user']);

    $this->entityTypeManager = $this->container->get('entity_type.manager');
    $this->pluginReferenceSelectionManager = $this->container->get('plugin.manager.plugin_reference_selection');
  }

  /**
   * Tests the ::getReferenceablePlugins method.
   *
   * @dataProvider getReferenceablePluginsDataProvider
   *
   * @covers ::getReferenceablePlugins
   */
  public function testGetReferenceablePlugins(array $expected, array $instance_options, ?string $match, string $match_operator, int $limit) {
    /** @var \Drupal\pluginreference\PluginReferenceSelectionInterface $plugin_reference_selection */
    $plugin_reference_selection = $this->pluginReferenceSelectionManager->getInstance($instance_options);
    $this->assertEquals($expected, array_keys($plugin_reference_selection->getReferenceablePlugins($match, $match_operator, $limit)));
  }

  /**
   * Data provider for testGetReferenceablePlugins().
   *
   * @see testGetReferenceablePlugins()
   */
  public static function getReferenceablePluginsDataProvider() {
    return [
      // Test filtering on plugin ID.
      [
        [
          'plugin_reference_autocomplete',
          'plugin_reference_options_buttons',
        ],
        [
          'target_type' => 'field.widget',
          'handler' => 'filtered',
          'filter' => [
            'key' => 'id',
            'negate' => FALSE,
            'target_values' => [
              'plugin_reference_autocomplete',
              'plugin_reference_options_buttons',
            ],
          ],
          'sort' => [
            'key' => 'label',
            'direction' => 'ASC',
          ],
        ],
        NULL,
        'CONTAINS',
        0,
      ],
      // Test negated filtering on plugin ID.
      [
        [
          'entity_reference_autocomplete',
          'entity_reference_autocomplete_tags',
          'options_buttons',
          'datetime_timestamp',
          'email_default',
          'language_select',
          'number',
          'plugin_reference_select',
          'options_select',
          'shape_only_color_editable_widget',
          'boolean_checkbox',
          'string_textarea',
          'string_textfield',
          'uri',
        ],
        [
          'target_type' => 'field.widget',
          'handler' => 'filtered',
          'filter' => [
            'key' => 'id',
            'negate' => TRUE,
            'target_values' => [
              'plugin_reference_autocomplete',
              'plugin_reference_options_buttons',
            ],
          ],
          'sort' => [
            'key' => 'label',
            'direction' => 'ASC',
          ],
        ],
        NULL,
        'CONTAINS',
        0,
      ],
      // Test filtering on plugin ID and sorting descending on label.
      [
        [
          'plugin_reference_options_buttons',
          'plugin_reference_autocomplete',
        ],
        [
          'target_type' => 'field.widget',
          'handler' => 'filtered',
          'filter' => [
            'key' => 'id',
            'negate' => FALSE,
            'target_values' => [
              'plugin_reference_autocomplete',
              'plugin_reference_options_buttons',
            ],
          ],
          'sort' => [
            'key' => 'label',
            'direction' => 'DESC',
          ],
        ],
        NULL,
        'CONTAINS',
        0,
      ],
      // Test filtering on plugin ID and sorting descending on label, the
      // label starts with filter and limit to 1 result.
      [
        [
          'options_select',
        ],
        [
          'target_type' => 'field.widget',
          'handler' => 'filtered',
          'filter' => [
            'key' => 'id',
            'negate' => FALSE,
            'target_values' => [
              'plugin_reference_autocomplete',
              'plugin_reference_options_buttons',
              'plugin_reference_select',
              'options_select',
            ],
          ],
          'sort' => [
            'key' => 'label',
            'direction' => 'DESC',
          ],
        ],
        'Select',
        'STARTS_WITH',
        1,
      ],
      // Test filtering on provider.
      [
        [
          'plugin_reference_autocomplete',
          'plugin_reference_options_buttons',
          'plugin_reference_select',
        ],
        [
          'target_type' => 'field.widget',
          'handler' => 'filtered',
          'filter' => [
            'key' => 'provider',
            'negate' => FALSE,
            'target_values' => [
              'pluginreference',
            ],
          ],
          'sort' => [
            'key' => 'label',
            'direction' => 'ASC',
          ],
        ],
        NULL,
        'CONTAINS',
        0,
      ],
      // Test negated filtering on provider.
      [
        [
          'entity_reference_autocomplete',
          'entity_reference_autocomplete_tags',
          'options_buttons',
          'datetime_timestamp',
          'email_default',
          'language_select',
          'number',
          'options_select',
          'shape_only_color_editable_widget',
          'boolean_checkbox',
          'string_textarea',
          'string_textfield',
          'uri',
        ],
        [
          'target_type' => 'field.widget',
          'handler' => 'filtered',
          'filter' => [
            'key' => 'provider',
            'negate' => TRUE,
            'target_values' => [
              'pluginreference',
            ],
          ],
          'sort' => [
            'key' => 'label',
            'direction' => 'ASC',
          ],
        ],
        NULL,
        'CONTAINS',
        0,
      ],
      // Test filtering on block category.
      [
        [
          'plugin_reference_test_block',
          'plugin_reference_test_caching_block',
        ],
        [
          'target_type' => 'block',
          'handler' => 'filtered',
          'filter' => [
            'key' => 'category',
            'negate' => FALSE,
            'target_values' => [
              'Plugin Reference Test',
            ],
          ],
          'sort' => [
            'key' => 'label',
            'direction' => 'ASC',
          ],
        ],
        NULL,
        'CONTAINS',
        0,
      ],
    ];
  }

  /**
   * Tests the ::countReferenceablePlugins method.
   *
   * @dataProvider countReferenceablePluginsDataProvider
   *
   * @covers ::countReferenceablePlugins
   */
  public function testCountReferenceablePlugins(int $expected, array $instance_options, ?string $match, string $match_operator) {
    /** @var \Drupal\pluginreference\PluginReferenceSelectionInterface $plugin_reference_selection */
    $plugin_reference_selection = $this->pluginReferenceSelectionManager->getInstance($instance_options);
    $this->assertEquals($expected, $plugin_reference_selection->countReferenceablePlugins($match, $match_operator));
  }

  /**
   * Data provider for testCountReferenceablePlugins().
   *
   * @see testCountReferenceablePlugins()
   */
  public static function countReferenceablePluginsDataProvider() {
    return [
      // Test count on a filtered list on plugin ID.
      [
        2,
        [
          'target_type' => 'field.widget',
          'handler' => 'filtered',
          'filter' => [
            'key' => 'id',
            'negate' => FALSE,
            'target_values' => [
              'plugin_reference_autocomplete',
              'plugin_reference_options_buttons',
            ],
          ],
          'sort' => [
            'key' => 'label',
            'direction' => 'ASC',
          ],
        ],
        NULL,
        'CONTAINS',
      ],
      // Test count on a negated filtered list on plugin ID.
      [
        14,
        [
          'target_type' => 'field.widget',
          'handler' => 'filtered',
          'filter' => [
            'key' => 'id',
            'negate' => TRUE,
            'target_values' => [
              'plugin_reference_autocomplete',
              'plugin_reference_options_buttons',
            ],
          ],
          'sort' => [
            'key' => 'label',
            'direction' => 'ASC',
          ],
        ],
        NULL,
        'CONTAINS',
      ],
      // Test count on a filtered list on plugin ID with a STARTS_WITH filter.
      [
        0,
        [
          'target_type' => 'field.widget',
          'handler' => 'filtered',
          'filter' => [
            'key' => 'id',
            'negate' => FALSE,
            'target_values' => [
              'plugin_reference_autocomplete',
              'plugin_reference_options_buttons',
            ],
          ],
          'sort' => [
            'key' => 'label',
            'direction' => 'ASC',
          ],
        ],
        'Select',
        'STARTS_WITH',
      ],
      // Test count on a filtered list on provider.
      [
        3,
        [
          'target_type' => 'field.widget',
          'handler' => 'filtered',
          'filter' => [
            'key' => 'provider',
            'negate' => FALSE,
            'target_values' => [
              'pluginreference',
            ],
          ],
          'sort' => [
            'key' => 'label',
            'direction' => 'ASC',
          ],
        ],
        NULL,
        'CONTAINS',
      ],
      // Test count on a filtered list on block category.
      [
        2,
        [
          'target_type' => 'block',
          'handler' => 'filtered',
          'filter' => [
            'key' => 'category',
            'negate' => FALSE,
            'target_values' => [
              'Plugin Reference Test',
            ],
          ],
          'sort' => [
            'key' => 'label',
            'direction' => 'ASC',
          ],
        ],
        NULL,
        'CONTAINS',
      ],
    ];
  }

  /**
   * Tests the ::validateReferenceablePlugins method.
   *
   * @dataProvider validateReferenceablePluginsDataProvider
   *
   * @covers ::validateReferenceablePlugins
   */
  public function testValidateReferenceablePlugins(array $expected, array $instance_options, array $plugin_ids) {
    /** @var \Drupal\pluginreference\PluginReferenceSelectionInterface $plugin_reference_selection */
    $plugin_reference_selection = $this->pluginReferenceSelectionManager->getInstance($instance_options);
    $this->assertEquals($expected, $plugin_reference_selection->validateReferenceablePlugins($plugin_ids));
  }

  /**
   * Data provider for testValidateReferenceablePlugins().
   *
   * @see testValidateReferenceablePlugins()
   */
  public static function validateReferenceablePluginsDataProvider() {
    return [
      // Test validation on a filtered list on plugin ID.
      [
        [
          'plugin_reference_autocomplete',
          'plugin_reference_options_buttons',
        ],
        [
          'target_type' => 'field.widget',
          'handler' => 'filtered',
          'filter' => [
            'key' => 'id',
            'negate' => FALSE,
            'target_values' => [
              'plugin_reference_autocomplete',
              'plugin_reference_options_buttons',
            ],
          ],
          'sort' => [
            'key' => 'label',
            'direction' => 'ASC',
          ],
        ],
        [
          'plugin_reference_autocomplete',
          'plugin_reference_options_buttons',
          'nonexistent_widget',
          'options_select',
        ],
      ],
      // Test validation on a negated filtered list on provider.
      [
        [
          'options_select',
        ],
        [
          'target_type' => 'field.widget',
          'handler' => 'filtered',
          'filter' => [
            'key' => 'provider',
            'negate' => TRUE,
            'target_values' => [
              'pluginreference',
            ],
          ],
          'sort' => [
            'key' => 'label',
            'direction' => 'ASC',
          ],
        ],
        [
          'plugin_reference_autocomplete',
          'plugin_reference_options_buttons',
          'nonexistent_widget',
          'options_select',
        ],
      ],
      // Test validation on a filtered list on block category.
      [
        [
          'plugin_reference_test_block',
          'plugin_reference_test_caching_block',
        ],
        [
          'target_type' => 'block',
          'handler' => 'filtered',
          'filter' => [
            'key' => 'category',
            'negate' => FALSE,
            'target_values' => [
              'Plugin Reference Test',
            ],
          ],
          'sort' => [
            'key' => 'label',
            'direction' => 'ASC',
          ],
        ],
        [
          'plugin_reference_test_access_block',
          'plugin_reference_test_block',
          'plugin_reference_test_caching_block',
          'nonexistent_widget',
        ],
      ],
    ];
  }

  /**
   * Test the overridden FilteredSelection for block plugins.
   */
  public function testBlockFilteredSelection() {
    /** @var \Drupal\pluginreference\PluginReferenceSelectionInterface $plugin_reference_selection */
    $options = [
      'target_type' => 'block',
      'handler' => 'filtered',
      'filter' => [
        'key' => 'id',
        'negate' => FALSE,
        'target_values' => [
          'plugin_reference_test_access_block',
          'plugin_reference_test_block',
          'plugin_reference_test_caching_block',
        ],
      ],
      'sort' => [
        'key' => 'label',
        'direction' => 'ASC',
      ],
    ];
    $plugin_reference_selection = $this->pluginReferenceSelectionManager->getInstance($options);
    $this->assertInstanceOf(BlockFilteredSelection::class, $plugin_reference_selection);

    $this->assertEmpty($plugin_reference_selection->getReferenceablePlugins('Test access block'));
    $this->assertEquals(0, $plugin_reference_selection->countReferenceablePlugins('Test access block'));
    $this->assertEmpty($plugin_reference_selection->validateReferenceablePlugins(['plugin_reference_test_access_block']));

    $user_role_storage = $this->entityTypeManager->getStorage('user_role');
    $anonymous_role = $user_role_storage->load(RoleInterface::ANONYMOUS_ID);
    $anonymous_role->grantPermission('view plugin reference test access block');
    $anonymous_role->save();

    $this->assertEquals(['plugin_reference_test_access_block'], array_keys($plugin_reference_selection->getReferenceablePlugins('Test access block')));
    $this->assertEquals(1, $plugin_reference_selection->countReferenceablePlugins('Test access block'));
    $this->assertEquals(['plugin_reference_test_access_block'], $plugin_reference_selection->validateReferenceablePlugins(['plugin_reference_test_access_block']));
  }

}
