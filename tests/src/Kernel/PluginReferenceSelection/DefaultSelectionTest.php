<?php

namespace Drupal\Tests\pluginreference\Kernel\PluginReferenceSelection;

use Drupal\KernelTests\KernelTestBase;
use Drupal\pluginreference\Plugin\PluginReferenceSelection\BlockSelection;
use Drupal\user\RoleInterface;

/**
 * Tests the Plugin reference DefaultSelection plugin.
 *
 * @group pluginreference
 *
 * @coversDefaultClass \Drupal\pluginreference\Plugin\PluginReferenceSelection\DefaultSelection
 */
class DefaultSelectionTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'pluginreference',
    'pluginreference_test',
    'entity_test',
    'field',
    'user',
    'system',
  ];

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The plugin reference selection manager.
   *
   * @var \Drupal\pluginreference\PluginReferenceSelectionManagerInterface
   */
  protected $pluginReferenceSelectionManager;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('entity_test');
    $this->installConfig(['field', 'user']);

    $this->entityTypeManager = $this->container->get('entity_type.manager');
    $this->pluginReferenceSelectionManager = $this->container->get('plugin.manager.plugin_reference_selection');
  }

  /**
   * Tests the ::getReferenceablePlugins method.
   *
   * @dataProvider getReferenceablePluginsDataProvider
   *
   * @covers ::getReferenceablePlugins
   */
  public function testGetReferenceablePlugins(array $expected, array $instance_options, ?string $match, string $match_operator, int $limit) {
    /** @var \Drupal\pluginreference\PluginReferenceSelectionInterface $plugin_reference_selection */
    $plugin_reference_selection = $this->pluginReferenceSelectionManager->getInstance($instance_options);
    $this->assertEquals($expected, array_keys($plugin_reference_selection->getReferenceablePlugins($match, $match_operator, $limit)));
  }

  /**
   * Data provider for testGetReferenceablePlugins().
   *
   * @see testGetReferenceablePlugins()
   */
  public static function getReferenceablePluginsDataProvider() {
    return [
      [
        [
          'plugin_reference_autocomplete',
          'entity_reference_autocomplete',
          'entity_reference_autocomplete_tags',
          'plugin_reference_options_buttons',
          'options_buttons',
          'datetime_timestamp',
          'email_default',
          'language_select',
          'number',
          'plugin_reference_select',
          'options_select',
          'shape_only_color_editable_widget',
          'boolean_checkbox',
          'string_textarea',
          'string_textfield',
          'uri',
        ],
        [
          'target_type' => 'field.widget',
          'sort' => [
            'key' => 'label',
            'direction' => 'ASC',
          ],
        ],
        NULL,
        'CONTAINS',
        0,
      ],
      [
        [
          'uri',
          'string_textfield',
          'string_textarea',
          'boolean_checkbox',
          'shape_only_color_editable_widget',
          'options_select',
          'plugin_reference_select',
          'number',
          'language_select',
          'email_default',
          'datetime_timestamp',
          'options_buttons',
          'plugin_reference_options_buttons',
          'entity_reference_autocomplete_tags',
          'entity_reference_autocomplete',
          'plugin_reference_autocomplete',
        ],
        [
          'target_type' => 'field.widget',
          'sort' => [
            'key' => 'label',
            'direction' => 'DESC',
          ],
        ],
        NULL,
        'CONTAINS',
        0,
      ],
      [
        [
          'language_select',
          'plugin_reference_select',
          'options_select',
        ],
        [
          'target_type' => 'field.widget',
          'sort' => [
            'key' => 'label',
            'direction' => 'ASC',
          ],
        ],
        'Select',
        'CONTAINS',
        0,
      ],
      [
        [
          'plugin_reference_select',
          'options_select',
        ],
        [
          'target_type' => 'field.widget',
          'sort' => [
            'key' => 'label',
            'direction' => 'ASC',
          ],
        ],
        'Select',
        'STARTS_WITH',
        0,
      ],
      [
        [
          'plugin_reference_autocomplete',
          'entity_reference_autocomplete',
          'entity_reference_autocomplete_tags',
        ],
        [
          'target_type' => 'field.widget',
          'sort' => [
            'key' => 'label',
            'direction' => 'ASC',
          ],
        ],
        NULL,
        'CONTAINS',
        3,
      ],
    ];
  }

  /**
   * Tests the ::countReferenceablePlugins method.
   *
   * @dataProvider countReferenceablePluginsDataProvider
   *
   * @covers ::countReferenceablePlugins
   */
  public function testCountReferenceablePlugins(int $expected, array $instance_options, ?string $match, string $match_operator) {
    /** @var \Drupal\pluginreference\PluginReferenceSelectionInterface $plugin_reference_selection */
    $plugin_reference_selection = $this->pluginReferenceSelectionManager->getInstance($instance_options);
    $this->assertEquals($expected, $plugin_reference_selection->countReferenceablePlugins($match, $match_operator));
  }

  /**
   * Data provider for testCountReferenceablePlugins().
   *
   * @see testCountReferenceablePlugins()
   */
  public static function countReferenceablePluginsDataProvider() {
    return [
      [
        16,
        [
          'target_type' => 'field.widget',
          'sort' => [
            'key' => 'label',
            'direction' => 'ASC',
          ],
        ],
        NULL,
        'CONTAINS',
      ],
      [
        3,
        [
          'target_type' => 'field.widget',
          'sort' => [
            'key' => 'label',
            'direction' => 'ASC',
          ],
        ],
        'Select',
        'CONTAINS',
      ],
      [
        2,
        [
          'target_type' => 'field.widget',
          'sort' => [
            'key' => 'label',
            'direction' => 'ASC',
          ],
        ],
        'Select',
        'STARTS_WITH',
      ],
    ];
  }

  /**
   * Tests the ::validateReferenceablePlugins method.
   *
   * @dataProvider validateReferenceablePluginsDataProvider
   *
   * @covers ::validateReferenceablePlugins
   */
  public function testValidateReferenceablePlugins(array $expected, array $instance_options, array $plugin_ids) {
    /** @var \Drupal\pluginreference\PluginReferenceSelectionInterface $plugin_reference_selection */
    $plugin_reference_selection = $this->pluginReferenceSelectionManager->getInstance($instance_options);
    $this->assertEquals($expected, $plugin_reference_selection->validateReferenceablePlugins($plugin_ids));
  }

  /**
   * Data provider for testValidateReferenceablePlugins().
   *
   * @see testValidateReferenceablePlugins()
   */
  public static function validateReferenceablePluginsDataProvider() {
    return [
      [
        [
          'plugin_reference_autocomplete',
          'plugin_reference_options_buttons',
        ],
        [
          'target_type' => 'field.widget',
          'sort' => [
            'key' => 'label',
            'direction' => 'ASC',
          ],
        ],
        [
          'plugin_reference_autocomplete',
          'plugin_reference_options_buttons',
          'nonexistent_widget',
        ],
      ],
    ];
  }

  /**
   * Test the overridden DefaultSelection for block plugins.
   */
  public function testDefaultBlockSelection() {
    /** @var \Drupal\pluginreference\PluginReferenceSelectionInterface $plugin_reference_selection */
    $plugin_reference_selection = $this->pluginReferenceSelectionManager->getInstance(['target_type' => 'block']);
    $this->assertInstanceOf(BlockSelection::class, $plugin_reference_selection);

    $this->assertEmpty($plugin_reference_selection->getReferenceablePlugins('Test access block'));
    $this->assertEquals(0, $plugin_reference_selection->countReferenceablePlugins('Test access block'));
    $this->assertEmpty($plugin_reference_selection->validateReferenceablePlugins(['plugin_reference_test_access_block']));

    $user_role_storage = $this->entityTypeManager->getStorage('user_role');
    $anonymous_role = $user_role_storage->load(RoleInterface::ANONYMOUS_ID);
    $anonymous_role->grantPermission('view plugin reference test access block');
    $anonymous_role->save();

    $this->assertEquals(['plugin_reference_test_access_block'], array_keys($plugin_reference_selection->getReferenceablePlugins('Test access block')));
    $this->assertEquals(1, $plugin_reference_selection->countReferenceablePlugins('Test access block'));
    $this->assertEquals(['plugin_reference_test_access_block'], $plugin_reference_selection->validateReferenceablePlugins(['plugin_reference_test_access_block']));
  }

}
