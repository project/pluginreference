<?php

namespace Drupal\KernelTests\Core\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\Tests\pluginreference\Traits\PluginReferenceTrait;
use Drupal\user\Entity\Role;
use Drupal\user\Entity\User;

/**
 * Tests validation constraints for ValidPluginReferenceConstraintValidator.
 *
 * @group pluginreference
 */
class PluginReferenceValidPluginReferenceConstraintValidatorTest extends EntityKernelTestBase {

  use PluginReferenceTrait;

  /**
   * The typed data manager to use.
   *
   * @var \Drupal\Core\TypedData\TypedDataManager
   */
  protected $typedData;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'pluginreference',
    'pluginreference_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->typedData = $this->container->get('typed_data_manager');
  }

  /**
   * Tests the ValidReferenceConstraintValidator.
   */
  public function testValidation() {
    $definition = BaseFieldDefinition::create('plugin_reference')
      ->setSettings(['target_type' => 'block']);

    // Test that validation passes with a valid value.
    $typed_data = $this->typedData->create($definition, ['plugin_id' => 'plugin_reference_test_block']);
    $violations = $typed_data->validate();
    $this->assertEquals(0, $violations->count(), 'Validation passed for correct value.');

    // NULL is also considered a valid reference.
    $typed_data = $this->typedData->create($definition, ['plugin_id' => NULL]);
    $violations = $typed_data->validate();
    $this->assertEquals(0, $violations->count(), 'Validation passed for correct value.');

    // Check that validation fails when an nonexistent plugin is passed.
    $typed_data = $this->typedData->create($definition, ['plugin_id' => 'nonexistent_plugin']);
    $violations = $typed_data->validate();
    $this->assertGreaterThan(0, $violations->count(), 'Validation failed for incorrect value.');

    // Make sure the information provided by a violation is correct.
    $this->assertEquals(t('The referenced plugin (%type: %id) does not exist.', [
      '%type' => 'block',
      '%id' => 'nonexistent_plugin',
    ]), $violations[0]->getMessage(), 'The message for invalid value is correct.');
    $this->assertEquals($typed_data, $violations[0]->getRoot(), 'Violation root is correct.');
  }

  /**
   * Tests the validation of pre-existing items in a plugin reference field.
   */
  public function testPreExistingItemsValidation() {
    // Create two types of users, with and without access to the test block.
    /** @var \Drupal\user\RoleInterface $role_with_access */
    $role_with_access = Role::create(['id' => 'role_with_access', 'label' => 'Role with access']);
    $role_with_access->grantPermission('view plugin reference test access block');
    $role_with_access->save();

    /** @var \Drupal\user\RoleInterface $role_without_access */
    $role_without_access = Role::create(['id' => 'role_without_access', 'label' => 'Role without access']);
    $role_without_access->save();

    $user_with_access = User::create(['roles' => ['role_with_access']]);
    $user_without_access = User::create(['roles' => ['role_without_access']]);

    // Add a plugin reference field.
    $this->createPluginReferenceField(
      'entity_test',
      'entity_test',
      'field_test',
      'Test field',
      'block',
      'default',
      [],
      FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED
    );

    // Plugin that can be referenced by any user.
    $plugin_id = 'system_branding_block';

    // Plugin that can be referenced by any user, provided by
    // pluginreference_test.
    $pluginreference_test_id = 'plugin_reference_test_block';

    // Plugin that has an access check implemented.
    $pluginreference_test_access_plugin_id = 'plugin_reference_test_access_block';

    $referencing_entity = EntityTest::create([
      'field_test' => [
        ['plugin_id' => $plugin_id],
        ['plugin_id' => $pluginreference_test_id],
        ['plugin_id' => $pluginreference_test_access_plugin_id],
      ],
    ]);

    // Check that users with access are able pass the validation for fields
    // without pre-existing content.
    $this->container->get('account_switcher')->switchTo($user_with_access);

    $violations = $referencing_entity->field_test->validate();
    $this->assertCount(0, $violations);

    // Check that users without access are not able pass the validation for
    // fields without pre-existing content.
    $this->container->get('account_switcher')->switchTo($user_without_access);

    $violations = $referencing_entity->field_test->validate();
    $this->assertCount(1, $violations);
    $this->assertEquals(t('This plugin (%type: %id) cannot be referenced.', [
      '%type' => 'block',
      '%id' => $pluginreference_test_access_plugin_id,
    ]), $violations[0]->getMessage());

    // Now save the referencing entity which will create a pre-existing state
    // for it and repeat the checks. This time, the user without access should
    // be able to pass the validation as well because it's not changing the
    // pre-existing state.
    $referencing_entity->save();

    $this->container->get('account_switcher')->switchTo($user_with_access);

    $violations = $referencing_entity->field_test->validate();
    $this->assertCount(0, $violations);

    // Check that users without access are able pass the validation for fields
    // with pre-existing content.
    $this->container->get('account_switcher')->switchTo($user_without_access);

    $violations = $referencing_entity->field_test->validate();
    $this->assertCount(0, $violations);

    // Re-save the referencing entity and check that the referenced plugins are
    // not affected.
    $referencing_entity->name->value = $this->randomString();
    $referencing_entity->save();
    $this->assertEquals($plugin_id, $referencing_entity->field_test[0]->plugin_id);
    $this->assertEquals($pluginreference_test_id, $referencing_entity->field_test[1]->plugin_id);
    $this->assertEquals($pluginreference_test_access_plugin_id, $referencing_entity->field_test[2]->plugin_id);

    $violations = $referencing_entity->field_test->validate();
    $this->assertCount(0, $violations);

    // @todo check when switching of handler that the plugin is no longer
    // referenceable.
    // Uninstall the pluginreference_test module and check that the
    // pre-existing references are not valid anymore.
    $this->uninstallModule('pluginreference_test');
    $referencing_entity = $this->reloadEntity($referencing_entity);
    $violations = $referencing_entity->field_test->validate();

    $this->assertCount(2, $violations);
    $this->assertEquals(t('The referenced plugin (%type: %id) does not exist.', [
      '%type' => 'block',
      '%id' => $pluginreference_test_id,
    ]), $violations[0]->getMessage());
    $this->assertEquals(t('The referenced plugin (%type: %id) does not exist.', [
      '%type' => 'block',
      '%id' => $pluginreference_test_access_plugin_id,
    ]), $violations[1]->getMessage());
  }

}
