<?php

namespace Drupal\Tests\pluginreference\Functional;

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\SchemaCheckTestTrait;

/**
 * Tests plugin reference field default values storage in CMI.
 *
 * @group entity_reference
 */
class PluginReferenceDefaultValueTest extends BrowserTestBase {

  use SchemaCheckTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'image',
    'comment',
    'field_ui',
    'node',
    'pluginreference_test',
    'system',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The module installer.
   *
   * @var \Drupal\Core\Extension\ModuleInstallerInterface
   */
  protected $moduleInstaller;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create default content type.
    $this->drupalLogin($this->rootUser);
    $this->drupalCreateContentType(['type' => 'page']);
    $this->moduleInstaller = $this->container->get('module_installer');
  }

  /**
   * Tests that default values are correctly stored.
   */
  public function testPluginReferenceDefaultValue() {
    $page = $this->getSession()->getPage();

    $field_name = mb_strtolower($this->randomMachineName());
    $field_storage = FieldStorageConfig::create([
      'field_name' => $field_name,
      'entity_type' => 'node',
      'type' => 'plugin_reference',
      'settings' => ['target_type' => 'field.widget'],
      'cardinality' => FieldStorageConfig::CARDINALITY_UNLIMITED,
    ]);
    $field_storage->save();
    $field = FieldConfig::create([
      'field_name' => $field_name,
      'entity_type' => 'node',
      'bundle' => 'page',
      'label' => $field_name,
      'settings' => [
        'handler' => 'default:field_widget_test',
      ],
    ]);
    $field->save();

    $this->drupalGet(sprintf('admin/structure/types/manage/page/fields/node.page.%s', $field_name));
    $this->submitForm([
      'set_default_value' => TRUE,
      sprintf('default_value_input[%s][0][plugin_id]', $field_name) => 'text_textarea',
    ], 'Save settings');

    // Check that default value is selected in default value form.
    $this->drupalGet('admin/structure/types/manage/page/fields/node.page.' . $field_name);
    $default_value_field = $page->findField(sprintf('default_value_input[%s][0][plugin_id]', $field_name));
    $this->assertEquals('text_textarea', $default_value_field->getValue());
  }

  /**
   * Test that dependencies of the pluginreference field can be removed.
   *
   * @see \Drupal\pluginreference\Plugin\Field\FieldType\PluginReferenceItem::onDependencyRemoval()
   */
  public function testPluginReferenceFieldDependenciesRemoval() {
    $field_name = mb_strtolower($this->randomMachineName());
    $field_storage = FieldStorageConfig::create([
      'field_name' => $field_name,
      'entity_type' => 'node',
      'type' => 'plugin_reference',
      'settings' => ['target_type' => 'field.widget'],
      'cardinality' => FieldStorageConfig::CARDINALITY_UNLIMITED,
    ]);
    $field_storage->save();
    $field = FieldConfig::create([
      'field_name' => $field_name,
      'entity_type' => 'node',
      'bundle' => 'page',
      'label' => $field_name,
      'settings' => [
        'handler' => 'field_widget_advanced',
      ],
      'default_value' => [
        [
          'plugin_id' => 'comment_default',
          'configuration' => ['title' => 'Test widget'],
        ],
        [
          'plugin_id' => 'plugin_reference_autocomplete',
          'configuration' => [],
        ],
      ],
    ]);
    $field->save();

    // Check that the default value is present before we uninstall the book
    // module.
    $field = FieldConfig::loadByName('node', 'page', $field_name);
    $this->assertEquals([
      [
        'plugin_id' => 'comment_default',
        'configuration' => ['title' => 'Test widget'],
      ],
      [
        'plugin_id' => 'plugin_reference_autocomplete',
        'configuration' => [],
      ],
    ], $field->getDefaultValueLiteral());

    $this->moduleInstaller->uninstall(['comment']);

    // Check that the default value was removed because the field widget plugin
    // no longer exists.
    $field = FieldConfig::loadByName('node', 'page', $field_name);
    $this->assertEquals([
      [
        'plugin_id' => 'plugin_reference_autocomplete',
        'configuration' => [],
      ],
    ], $field->getDefaultValueLiteral());

    // Check that the handler isset before we remove the pluginreference_test
    // module.
    $this->assertEquals('field_widget_advanced', $field->getSetting('handler'));
    $this->assertEquals([
      'config' => [
        sprintf('field.storage.node.%s', $field_name),
        'node.type.page',
      ],
      'module' => ['pluginreference', 'pluginreference_test'],
    ], $field->getDependencies());

    $this->moduleInstaller->uninstall(['pluginreference_test']);

    // The handler was provided by the pluginreference_test module so it should
    // fallback to the default and the dependency should be removed.
    $field = FieldConfig::loadByName('node', 'page', $field_name);
    $this->assertEquals('default:field.widget', $field->getSetting('handler'));
    $this->assertEquals([
      'config' => [
        sprintf('field.storage.node.%s', $field_name),
        'node.type.page',
      ],
      'module' => ['pluginreference'],
    ], $field->getDependencies());

    // Create a new field, referencing aggregator parsers.
    $field_name = mb_strtolower($this->randomMachineName());
    $field_storage = FieldStorageConfig::create([
      'field_name' => $field_name,
      'entity_type' => 'node',
      'type' => 'plugin_reference',
      'settings' => ['target_type' => 'image.effect'],
      'cardinality' => FieldStorageConfig::CARDINALITY_UNLIMITED,
    ]);
    $field_storage->save();
    $field = FieldConfig::create([
      'field_name' => $field_name,
      'entity_type' => 'node',
      'bundle' => 'page',
      'label' => $field_name,
      'settings' => [
        'handler' => 'default',
      ],
    ]);
    $field->save();

    // Check that the aggregator module was successfully added to the
    // field_storage.
    $field_storage = FieldStorageConfig::loadByName('node', $field_name);
    $this->assertEquals([
      'module' => [
        'image',
        'node',
        'pluginreference',
      ],
    ], $field_storage->getDependencies());

    $this->moduleInstaller->uninstall(['image']);

    // When the aggregator module is uninstalled, this field should be removed.
    $field_storage = FieldStorageConfig::loadByName('node', $field_name);
    $field = FieldConfig::loadByName('node', 'page', $field_name);
    $this->assertEmpty($field_storage);
    $this->assertEmpty($field);
  }

}
