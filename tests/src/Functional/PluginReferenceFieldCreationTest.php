<?php

namespace Drupal\Tests\pluginreference\Functional;

use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\field_ui\Traits\FieldUiTestTrait;

/**
 * Test the creation of an plugin reference in the UI.
 *
 * @group pluginreference
 */
class PluginReferenceFieldCreationTest extends BrowserTestBase {

  use FieldUiTestTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['block', 'field_ui', 'node', 'pluginreference'];

  /**
   * The module installer.
   *
   * @var \Drupal\Core\Extension\ModuleInstallerInterface
   */
  protected $moduleInstaller;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->drupalPlaceBlock('system_breadcrumb_block');
    $this->drupalLogin($this->rootUser);
    $this->drupalCreateContentType(['type' => 'page']);
    $this->moduleInstaller = $this->container->get('module_installer');
  }

  /**
   * Test the creation of a plugin reference field in the UI.
   */
  public function testPluginReferenceFieldCreation() {
    $this->drupalGet('admin/structure/types/manage/page/fields/add-field');
    // Test the creation of the field.
    $this->fieldUIAddNewField(
      'admin/structure/types/manage/page',
      'plugin_reference',
      'Plugin reference',
      'field_ui:plugin_reference:block',
    );
  }

  /**
   * Test the creation of a plugin_reference field with a preselected plugin.
   */
  public function testPreselectedPluginReferenceFieldCreation() {
    $this->drupalGet('admin/structure/types/manage/page/fields/add-field');
    // Check that the preselected plugin options are present.
    $this->assertSession()->elementExists('css', 'input[name="new_storage_type"][value="plugin_reference"]');
    $this->submitForm([
      'new_storage_type' => 'plugin_reference',
    ], 'Continue');
    $this->assertSession()->elementExists('css', 'input[name="group_field_options_wrapper"][value="field_ui:plugin_reference:block"]');
    $this->assertSession()->elementExists('css', 'input[name="group_field_options_wrapper"][value="field_ui:plugin_reference:filter"]');
    // Image effect is not available since the image module is not enabled.
    $this->assertSession()->elementNotExists('css', 'input[name="group_field_options_wrapper"][value="field_ui:plugin_reference:image.effect"]');

    $this->moduleInstaller->install(['image']);

    // Check that the image effect option is present now that the image module
    // is enabled.
    $this->drupalGet('admin/structure/types/manage/page/fields/add-field');
    $this->submitForm([
      'new_storage_type' => 'plugin_reference',
    ], 'Continue');
    $this->assertSession()->elementExists('css', 'input[name="group_field_options_wrapper"][value="field_ui:plugin_reference:image.effect"]');

    // Test the creation of the field.
    $this->fieldUIAddNewField(
      'admin/structure/types/manage/page',
      'plugin_reference',
      'Pluginreference',
      'field_ui:plugin_reference:block');

    // Check that the target type is correctly saved.
    $field_storage_config = FieldStorageConfig::loadByName('node', 'field_plugin_reference');
    $this->assertEquals('block', $field_storage_config->getSetting('target_type'));

    // Check that the target_type is correctly selected when a preselected
    // plugin is chosen.
    $this->drupalGet('admin/structure/types/manage/page/fields/add-field');
    $this->submitForm([
      'new_storage_type' => 'plugin_reference',
    ], 'Continue');
    $this->submitForm([
      'label' => 'Plugin reference',
      'field_name' => 'image_effect',
      'group_field_options_wrapper' => 'field_ui:plugin_reference:image.effect',
    ], 'Continue');
    $this->assertSession()->fieldValueEquals('field_storage[subform][settings][target_type]', 'image.effect');
  }

}
