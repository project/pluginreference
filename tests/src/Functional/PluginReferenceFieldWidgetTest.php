<?php

namespace Drupal\Tests\pluginreference\Functional;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Component\Utility\Crypt;
use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Site\Settings;
use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\pluginreference\Traits\PluginReferenceTrait;

/**
 * Test the plugin reference functionality.
 *
 * @group pluginreference
 */
class PluginReferenceFieldWidgetTest extends BrowserTestBase {

  use PluginReferenceTrait;

  /**
   * An admin in user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * The plugin reference field name.
   *
   * @var string
   */
  protected $fieldName = 'field_pluginreference';

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['node', 'field_ui', 'pluginreference'];

  /**
   * The node type.
   *
   * @var \Drupal\node\Entity\NodeType
   */
  protected $nodeType;

  /**
   * Permissions to grant admin user.
   *
   * @var array
   */
  protected $permissions = [
    'access content',
    'pluginreference autocomplete view results',
    'administer node fields',
    'administer node form display',
    'bypass node access',
  ];

  /**
   * The plugin type we want to reference.
   *
   * @var string
   */
  protected $targetType = 'field.widget';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->adminUser = $this->drupalCreateUser($this->permissions);
    $this->nodeType = $this->drupalCreateContentType();
    $this->createPluginReferenceField('node', $this->nodeType->id(), $this->fieldName, $this->randomMachineName(), $this->targetType,);
    // Make sure the index is created when a new plugin reference field is
    // added.
    $this->assertTrue($this->container->get('database')->schema()->indexExists('node__' . $this->fieldName, $this->fieldName . '_plugin_id'));
  }

  /**
   * Test pluginreference select widget with provider grouping.
   */
  public function testPluginReferenceSelectWidget() {
    $assert_session = $this->assertSession();
    EntityFormDisplay::load(sprintf('node.%s.default', $this->nodeType->id()))
      ->setComponent($this->fieldName, [
        'type' => 'plugin_reference_select',
        'settings' => [
          'provider_grouping' => TRUE,
          'configuration_form' => 'hidden',
        ],
      ])->save();

    $this->drupalLogin($this->adminUser);

    $this->drupalGet(Url::fromRoute('node.add', ['node_type' => $this->nodeType->id()]));

    $assert_session->fieldExists(sprintf('%s[0][plugin_id]', $this->fieldName));
    // Check that grouping per provider is enabled.
    $assert_session->elementExists('css', sprintf('select[name="%s[0][plugin_id]"] optgroup', $this->fieldName));
    $title = $this->randomMachineName();
    $edit = [
      'title[0][value]' => $title,
      sprintf('%s[0][plugin_id]', $this->fieldName) => 'text_textfield',
    ];
    $this->submitForm($edit, 'Save');
    $node = $this->drupalGetNodeByTitle($title);
    $assert_session->responseContains(new FormattableMarkup('@type %title has been created.', [
      '@type' => $this->nodeType->id(),
      '%title' => $node->toLink($node->label())->toString(),
    ]));
    $this->assertEquals($node->get($this->fieldName)
      ->offsetGet(0)->plugin_id, 'text_textfield');

    $this->drupalGet(Url::fromRoute('entity.node.edit_form', ['node' => $node->id()]));
    $assert_session->fieldValueEquals(sprintf('%s[0][plugin_id]', $this->fieldName), 'text_textfield');
  }

  /**
   * Test pluginreference select widget without provider grouping.
   */
  public function testPluginReferenceSelectWidgetWithoutProviderGrouping() {
    $assert_session = $this->assertSession();
    EntityFormDisplay::load(sprintf('node.%s.default', $this->nodeType->id()))
      ->setComponent($this->fieldName, [
        'type' => 'plugin_reference_select',
        'settings' => [
          'provider_grouping' => FALSE,
          'configuration_form' => 'hidden',
        ],
      ])->save();

    $this->drupalLogin($this->adminUser);

    $this->drupalGet(Url::fromRoute('node.add', ['node_type' => $this->nodeType->id()]));

    $assert_session->fieldExists(sprintf('%s[0][plugin_id]', $this->fieldName));
    // Check that grouping per provider is disabled.
    $assert_session->elementNotExists('css', sprintf('select[name="%s[0][plugin_id]"] optgroup', $this->fieldName));
    $title = $this->randomMachineName();
    $edit = [
      'title[0][value]' => $title,
      sprintf('%s[0][plugin_id]', $this->fieldName) => 'text_textfield',
    ];
    $this->submitForm($edit, 'Save');
    $node = $this->drupalGetNodeByTitle($title);
    $assert_session->responseContains(new FormattableMarkup('@type %title has been created.', [
      '@type' => $this->nodeType->id(),
      '%title' => $node->toLink($node->label())->toString(),
    ]));
    $this->assertEquals($node->get($this->fieldName)
      ->offsetGet(0)->plugin_id, 'text_textfield');

    $this->drupalGet(Url::fromRoute('entity.node.edit_form', ['node' => $node->id()]));
    $assert_session->fieldValueEquals(sprintf('%s[0][plugin_id]', $this->fieldName), 'text_textfield');
  }

  /**
   * Test pluginreference checkboxes widget.
   */
  public function testPluginReferenceOptionsButtonWidget() {
    $assert_session = $this->assertSession();
    EntityFormDisplay::load(sprintf('node.%s.default', $this->nodeType->id()))
      ->setComponent($this->fieldName, [
        'type' => 'plugin_reference_options_buttons',
      ])->save();

    $this->drupalLogin($this->adminUser);

    $this->drupalGet(Url::fromRoute('node.add', ['node_type' => $this->nodeType->id()]));

    $assert_session->fieldExists(sprintf('%s[plugin_id]', $this->fieldName));
    $title = $this->randomMachineName();
    $edit = [
      'title[0][value]' => $title,
      sprintf('%s[plugin_id]', $this->fieldName) => 'text_textfield',
    ];
    $this->submitForm($edit, 'Save');
    $node = $this->drupalGetNodeByTitle($title);
    $assert_session->responseContains(new FormattableMarkup('@type %title has been created.', [
      '@type' => $this->nodeType->id(),
      '%title' => $node->toLink($node->label())->toString(),
    ]));
    $this->assertEquals($node->get($this->fieldName)
      ->offsetGet(0)->plugin_id, 'text_textfield');

    $this->drupalGet(Url::fromRoute('entity.node.edit_form', ['node' => $node->id()]));
    $assert_session->fieldValueEquals(sprintf('%s[plugin_id]', $this->fieldName), 'text_textfield');
  }

  /**
   * Test plugin reference autocomplete widget.
   */
  public function testPluginReferenceAutocompleteWidget() {
    $assert_session = $this->assertSession();
    EntityFormDisplay::load(sprintf('node.%s.default', $this->nodeType->id()))
      ->setComponent($this->fieldName, [
        'type' => 'plugin_reference_autocomplete',
        'settings' => ['configuration_form' => 'hidden'],
      ])->save();

    $selection_settings = [];
    $selection_settings_key = Crypt::hmacBase64(serialize($selection_settings) . $this->targetType . 'default', Settings::getHashSalt());

    \Drupal::keyValue('plugin_autocomplete')->set($selection_settings_key, $selection_settings);

    $this->drupalGet(Url::fromRoute('pluginreference.plugin_autocomplete', [
      'target_type' => $this->targetType,
      'selection_handler' => 'default',
      'selection_settings_key' => $selection_settings_key,
      'q' => 'filter',
    ]));
    // Make sure a user without the right permissions can't access the
    // autocomplete page.
    $assert_session->statusCodeEquals(403);

    $this->drupalLogin($this->adminUser);

    $this->drupalGet(Url::fromRoute('pluginreference.plugin_autocomplete', [
      'target_type' => $this->targetType,
      'selection_handler' => 'default',
      'selection_settings_key' => $selection_settings_key,
      'q' => 'filter',
    ]));
    $assert_session->statusCodeEquals(200);

    // Check that a wrong selection_settings_key results in an access denied
    // error.
    $selection_settings[$this->randomMachineName()] = $this->randomString();
    \Drupal::keyValue('plugin_autocomplete')->set($selection_settings_key, $selection_settings);

    $this->drupalGet(Url::fromRoute('pluginreference.plugin_autocomplete', [
      'target_type' => $this->targetType,
      'selection_handler' => 'default',
      'selection_settings_key' => $selection_settings_key,
      'q' => 'filter',
    ]));
    $assert_session->statusCodeEquals(403);

    $this->drupalGet(Url::fromRoute('node.add', ['node_type' => $this->nodeType->id()]));
    $assert_session->fieldExists(sprintf('%s[0][plugin_id]', $this->fieldName));
    $title = $this->randomMachineName();
    $edit = [
      'title[0][value]' => $title,
      sprintf('%s[0][plugin_id]', $this->fieldName) => 'Text field (text_textfield)',
    ];
    $this->submitForm($edit, 'Save');
    $node = $this->drupalGetNodeByTitle($title);
    $assert_session->responseContains(new FormattableMarkup('@type %title has been created.', [
      '@type' => $this->nodeType->id(),
      '%title' => $node->toLink($node->label())->toString(),
    ]));
    $this->assertEquals($node->get($this->fieldName)->offsetGet(0)->plugin_id, 'text_textfield');

    $this->drupalGet(Url::fromRoute('entity.node.edit_form', ['node' => $node->id()]));
    $assert_session->fieldValueEquals(sprintf('%s[0][plugin_id]', $this->fieldName), 'Text field (text_textfield)');
  }

}
