<?php

namespace Drupal\Tests\pluginreference\Functional\Update;

use Drupal\FunctionalTests\Update\UpdatePathTestBase;

/**
 * Update test that checks if the index was added to the plugin_id column.
 *
 * @group pluginreference
 */
class PluginReferenceUpdate9002 extends UpdatePathTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'field',
    'block',
    'pluginreference',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setDatabaseDumpFiles() {
    $this->databaseDumpFiles = [
      DRUPAL_ROOT . '/core/modules/system/tests/fixtures/update/drupal-9.4.0.bare.standard.php.gz',
      __DIR__ . '/../../../fixtures/update/pluginreference-update-9000.php',
    ];
  }

  /**
   * Update test that checks if the index was added to the plugin_id column.
   *
   * @see pluginreference_update_9002()
   */
  public function testUpdate9002() {
    $this->assertFalse($this->container->get('database')->schema()->indexExists('node__field_block', 'field_block_plugin_id'));
    $this->assertFalse($this->container->get('database')->schema()->indexExists('node_revision__field_block', 'field_block_plugin_id'));
    $this->runUpdates();
    $this->assertTrue($this->container->get('database')->schema()->indexExists('node__field_block', 'field_block_plugin_id'));
    $this->assertTrue($this->container->get('database')->schema()->indexExists('node_revision__field_block', 'field_block_plugin_id'));
  }

}
