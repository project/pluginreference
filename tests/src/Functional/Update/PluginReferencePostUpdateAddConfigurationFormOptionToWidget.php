<?php

namespace Drupal\Tests\pluginreference\Functional\Update;

use Drupal\FunctionalTests\Update\UpdatePathTestBase;

/**
 * Update test that checks if configuration column was added.
 *
 * @group pluginreference
 */
class PluginReferencePostUpdateAddConfigurationFormOptionToWidget extends UpdatePathTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'field',
    'block',
    'pluginreference',
  ];

  /**
   * The entity form display storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $entityFormDisplayStorage;

  /**
   * {@inheritdoc}
   */
  protected function setDatabaseDumpFiles() {
    $this->databaseDumpFiles = [
      DRUPAL_ROOT . '/core/modules/system/tests/fixtures/update/drupal-9.4.0.bare.standard.php.gz',
      __DIR__ . '/../../../fixtures/update/pluginreference-update-9000.php',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->entityFormDisplayStorage = $this->container->get('entity_type.manager')->getStorage('entity_form_display');
  }

  /**
   * Update test that checks if the default configuration_form was added.
   *
   * @see pluginreference_post_update_add_configuration_form_option_to_widget()
   */
  public function testPostUpdateAddConfigurationFormOptionToWidget() {
    /** @var \Drupal\Core\Entity\Display\EntityFormDisplayInterface $entity_form_display */
    $entity_form_display = $this->entityFormDisplayStorage->loadUnchanged('node.page.default');
    $this->assertArrayNotHasKey('configuration_form', $entity_form_display->getComponent('field_block')['settings']);

    $this->runUpdates();

    $entity_form_display = $this->entityFormDisplayStorage->loadUnchanged('node.page.default');
    $this->assertEquals('hidden', $entity_form_display->getComponent('field_block')['settings']['configuration_form']);
  }

}
