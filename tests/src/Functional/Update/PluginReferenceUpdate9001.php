<?php

namespace Drupal\Tests\pluginreference\Functional\Update;

use Drupal\FunctionalTests\Update\UpdatePathTestBase;

/**
 * Update test that checks if the value property is converted to plugin_id.
 *
 * @group pluginreference
 * @group legacy
 */
class PluginReferenceUpdate9001 extends UpdatePathTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'field',
    'block',
    'pluginreference',
  ];

  /**
   * The node storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $nodeStorage;

  /**
   * {@inheritdoc}
   */
  protected function setDatabaseDumpFiles() {
    $this->databaseDumpFiles = [
      DRUPAL_ROOT . '/core/modules/system/tests/fixtures/update/drupal-9.4.0.bare.standard.php.gz',
      __DIR__ . '/../../../fixtures/update/pluginreference-update-9000.php',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->nodeStorage = $this->container->get('entity_type.manager')->getStorage('node');
  }

  /**
   * Update test that checks if the value property is converted to plugin_id.
   *
   * @see pluginreference_update_9001()
   */
  public function testUpdate9001() {
    $this->runUpdates();

    $node = $this->nodeStorage->load(1);
    $this->assertEquals('page_title_block', $node->get('field_block')->plugin_id);
  }

}
