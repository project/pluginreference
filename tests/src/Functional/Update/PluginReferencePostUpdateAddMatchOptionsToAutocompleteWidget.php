<?php

namespace Drupal\Tests\pluginreference\Functional\Update;

use Drupal\FunctionalTests\Update\UpdatePathTestBase;

/**
 * Update test; Check if the match options are added to the autocomplete widget.
 *
 * @group pluginreference
 */
class PluginReferencePostUpdateAddMatchOptionsToAutocompleteWidget extends UpdatePathTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'field',
    'block',
    'pluginreference',
  ];

  /**
   * The entity form display storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $entityFormDisplayStorage;

  /**
   * {@inheritdoc}
   */
  protected function setDatabaseDumpFiles() {
    $this->databaseDumpFiles = [
      DRUPAL_ROOT . '/core/modules/system/tests/fixtures/update/drupal-9.4.0.bare.standard.php.gz',
      __DIR__ . '/../../../fixtures/update/pluginreference-update-9000.php',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->entityFormDisplayStorage = $this->container->get('entity_type.manager')->getStorage('entity_form_display');
  }

  /**
   * Update test that checks if the default configuration_form was added.
   *
   * @see pluginreference_post_update_add_configuration_form_option_to_widget()
   */
  public function testPostUpdateAddConfigurationFormOptionToWidget() {
    /** @var \Drupal\Core\Entity\Display\EntityFormDisplayInterface $entity_form_display */
    $entity_form_display = $this->entityFormDisplayStorage->loadUnchanged('node.page.default');
    $this->assertArrayNotHasKey('match_operator', $entity_form_display->getComponent('field_block')['settings']);
    $this->assertArrayNotHasKey('match_limit', $entity_form_display->getComponent('field_block')['settings']);

    $this->runUpdates();

    $entity_form_display = $this->entityFormDisplayStorage->loadUnchanged('node.page.default');
    $this->assertEquals('CONTAINS', $entity_form_display->getComponent('field_block')['settings']['match_operator']);
    $this->assertEquals(10, $entity_form_display->getComponent('field_block')['settings']['match_limit']);
  }

}
