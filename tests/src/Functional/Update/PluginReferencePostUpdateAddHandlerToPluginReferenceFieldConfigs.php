<?php

namespace Drupal\Tests\pluginreference\Functional\Update;

use Drupal\FunctionalTests\Update\UpdatePathTestBase;

/**
 * Update test; Check if the match options are added to the autocomplete widget.
 *
 * @group pluginreference
 */
class PluginReferencePostUpdateAddHandlerToPluginReferenceFieldConfigs extends UpdatePathTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'field',
    'block',
    'pluginreference',
  ];

  /**
   * The field config storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $fieldConfigStorage;

  /**
   * {@inheritdoc}
   */
  protected function setDatabaseDumpFiles() {
    $this->databaseDumpFiles = [
      DRUPAL_ROOT . '/core/modules/system/tests/fixtures/update/drupal-9.4.0.bare.standard.php.gz',
      __DIR__ . '/../../../fixtures/update/pluginreference-update-9000.php',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->fieldConfigStorage = $this->container->get('entity_type.manager')->getStorage('field_config');
  }

  /**
   * Update test that checks if the default configuration_form was added.
   *
   * @see pluginreference_post_update_add_configuration_form_option_to_widget()
   */
  public function testPostUpdateAddConfigurationFormOptionToWidget() {
    /** @var \Drupal\field\FieldConfigInterface $field_config */
    $field_config = $this->fieldConfigStorage->loadUnchanged('node.page.field_block');
    $this->assertArrayNotHasKey('handler', $field_config->getSettings());
    $this->assertArrayNotHasKey('handler_settings', $field_config->getSettings());

    $this->runUpdates();

    $field_config = $this->fieldConfigStorage->loadUnchanged('node.page.field_block');
    $this->assertEquals('default:block', $field_config->getSetting('handler'));
    $this->assertEquals([
      'sort' => [
        'key' => 'label',
        'direction' => 'ASC',
      ],
    ], $field_config->getSetting('handler_settings'));
  }

}
