<?php

namespace Drupal\pluginreference_test\Plugin\PluginReferenceSelection;

use Drupal\pluginreference\Plugin\PluginReferenceSelection\DefaultSelection;

/**
 * Provides a plugin reference selection for field widgets.
 *
 * This class is used in the tests to check the weight and grouping.
 *
 * @PluginReferenceSelection(
 *   id = "default:field_widget_test",
 *   label = @Translation("Default"),
 *   plugin_types = {"field.widget"},
 *   group = "default",
 *   weight = 10
 * )
 */
class FieldWidgetTestSelection extends DefaultSelection {

}
