<?php

namespace Drupal\pluginreference_test\Plugin\PluginReferenceSelection;

use Drupal\pluginreference\Plugin\PluginReferenceSelection\DefaultSelection;

/**
 * Provides a plugin reference selection for field widgets.
 *
 * This class is used in the tests to check the weight and grouping.
 *
 * @PluginReferenceSelection(
 *   id = "field_widget_advanced",
 *   label = @Translation("Advanced widget"),
 *   plugin_types = {"field.widget"},
 *   group = "field_widget_advanced",
 *   weight = 0
 * )
 */
class FieldWidgetAdvancedSelection extends DefaultSelection {

}
