<?php

namespace Drupal\pluginreference_test\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;

/**
 * Provides a plugin reference test caching block.
 *
 * @Block(
 *   id = "plugin_reference_test_caching_block",
 *   admin_label = @Translation("Test caching block"),
 * )
 */
class PluginreferenceTestCachingBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return 'TEST';
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return Cache::mergeContexts(parent::getCacheContexts(), ['url.query_args']);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return Cache::mergeMaxAges(parent::getCacheMaxAge(), 3600);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    return Cache::mergeTags(parent::getCacheTags(), ['node_list']);
  }

}
