<?php

namespace Drupal\pluginreference_test\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Session\AccountInterface;

/**
 * Provides a plugin reference test caching block.
 *
 * @Block(
 *   id = "plugin_reference_test_access_block",
 *   admin_label = @Translation("Test access block"),
 * )
 */
class PluginReferenceTestAccessBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return 'TEST';
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'view plugin reference test access block');
  }

}
