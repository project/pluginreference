<?php

namespace Drupal\pluginreference_test\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a plugin reference test block.
 *
 * @Block(
 *   id = "plugin_reference_test_block",
 *   admin_label = @Translation("Test block"),
 * )
 */
class PluginreferenceTestBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'test_value' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return 'TEST';
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['test_value'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Test value'),
      '#default_value' => $this->configuration['test_value'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockValidate($form, FormStateInterface $form_state) {
    if ($form_state->getValue('test_value') === 'fail') {
      $form_state->setErrorByName('test_value', 'Value should not be fail.');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['test_value'] = $form_state->getValue('test_value');
  }

}
